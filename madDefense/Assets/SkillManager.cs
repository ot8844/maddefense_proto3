﻿using UnityEngine;
using System.Collections;

public class SkillManager : MonoBehaviour {
	/* user skill */
	public GameObject thunderBolt;
	public float thunderBoltDamage;
	public float thunderBoltRange;

	public PathTD path;



	void InitSkill(){
		ObjectPoolManager.New (thunderBolt, 20);
	}
	Vector3 GetPos(Vector3 point1, Vector3 point2){
		
		Vector3 temp1 = new Vector3 (point1.x, point1.y, 0);
		Vector3 temp2 = new Vector3 (point2.x, point2.y, 0);

		Vector3 dir=(temp1-temp2);

		float a = Random.Range (0.0f, 1.0f);
		//Vector3 result = point1 + a * (dir);
		//Debug.Log (a);
		return point1 - a * (dir);
	}
	IEnumerator HitTargetColor(UnitCreep tgt){
		tgt.creep.GetComponent<SpriteRenderer>().color = new Color(1F, 125.0f/255.0F, 125.0f/255.0F, 1F);
		yield return new WaitForSeconds(0.2f);
		tgt.creep.GetComponent<SpriteRenderer>().color = Color.white;
		yield return null;
		
	}
	public void HitTarget(UnitCreep tgt, float damage){
		if(tgt.gameObject!=null && tgt.gameObject.active){
			if(damage>0){
				StartCoroutine(HitTargetColor(tgt.unitC));
				tgt.ApplyDamage(damage);
			}
		}
	}

	IEnumerator ThunderBoltShot(){
		Transform[] waypoints = path.GetPath ();
		int i, j;
		for (i=0; i<15; i++) {
			Vector3 pos = GetPos (waypoints[i%4].position, waypoints[(i+1)%4].position);
			pos += new Vector3(0, 0, pos.y+100f);
			GameObject temp = ObjectPoolManager.Spawn(thunderBolt, pos, thunderBolt.transform.rotation);
			temp.GetComponent<ParticleSystem>().startSize *= Random.Range (0.7f, 1.5f);
			temp.GetComponent<ParticleSystem>().Play();
			LayerMask maskTarget = 1 << LayerManager.LayerCreep(); 
			Collider2D[] cols = Physics2D.OverlapCircleAll(temp.transform.position, thunderBoltRange, maskTarget);
			for (j=0; j<cols.Length; j++){
				HitTarget(cols[j].gameObject.GetComponent<UnitCreep>(), thunderBoltDamage);
			}
			yield return new WaitForSeconds(0.1f);
		}

		yield return null;
	}
	void ThunderBoltClick(){
		StartCoroutine (ThunderBoltShot ());
	}
	// Use this for initialization
	void Start () {
		InitSkill ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

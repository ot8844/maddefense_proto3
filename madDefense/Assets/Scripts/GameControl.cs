﻿using UnityEngine;
using System.Collections;

public enum _GameState{Idle, Started, Ended}

public class GameControl : MonoBehaviour {
	public float ResourceTime;

	public Object Item;
	public GameObject Table;
	public UIDraggableCamera viewCamera;
	public Sprite[] towerImages;
	public GameObject circleGauge;

	int nameCnt;
	static public _GameState gameState=_GameState.Idle;
	static public GameControl gameControl;

	public delegate void ResourceHandler(); 
	public static event ResourceHandler onResourceE;

	public int playerLife=20;
	public int playerResource=5;

	public int[] towerResource;
	public int[] buildTowerResource; // built tower's platform number -> index

	[HideInInspector] public LayerManager layerManager;

	private int currentWave=0;

	private bool towerResourceCheck=false;

	void Awake(){
		ObjectPoolManager.Init();
		gameControl=this;
		gameState=_GameState.Idle;
		towerResourceCheck = false;
		nameCnt = 5;

		for (int i=0; i<buildTowerResource.Length; i++){
			buildTowerResource[i] = -1;
		}

	}
	void OnEnable(){
		SpawnManager.onWaveStartSpawnE += OnWaveStartSpawned;

		UnitCreep.onLifeInE += OnInductLife;
		UnitCreep.onLifeDeE += OnDeductLife;
	}
	void OnDisable(){
		SpawnManager.onWaveStartSpawnE -= OnWaveStartSpawned;

		UnitCreep.onLifeInE -= OnInductLife;
		UnitCreep.onLifeDeE -= OnDeductLife;
	}

	void OnInductLife(int waveID){
		playerLife+=1;
	}
	
	void OnDeductLife(int waveID){
		playerLife-=1;
	}


	// Use this for initialization
	void Start () {
		MixTableInit ();
		ItemInit ();
		for (int i=0; i<6; i++){
			AddTower (i);
		}

	}

	public static void onGameOver(bool flag){
		UI.winLostFlag=flag;
		gameState = _GameState.Ended;
	}

	// Update is called once per frame
	void Update () {
		if (gameState==_GameState.Started && !towerResourceCheck){
			towerResourceCheck = true;
			StartCoroutine(GainTowerResource());


		}
		if (gameState==_GameState.Started){
			if (circleGauge.GetComponentInChildren<UIFilledSprite> ().fillAmount >= 0) {
				circleGauge.GetComponentInChildren<UIFilledSprite> ().fillAmount -= Time.deltaTime/ResourceTime;
			}
			//CoinClick();
		}

		/* ngui */

	}

	public static void RemoveTower(Item button){
		gameControl.towerResource [button.towerID]--;
		if (gameControl.towerResource [button.towerID] == 0) {
			button.ButtonDestroy();
		} else {
			button.LabelCount (gameControl.towerResource [button.towerID]);
		}
	}
	void AddButton(int towerID){
		GameObject newObject = Instantiate(Item) as GameObject;
		newObject.transform.parent = Table.transform;
		newObject.GetComponent<UIDragCamera> ().draggableCamera = viewCamera;
		newObject.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		nameCnt++;
		newObject.name = nameCnt.ToString ();
		newObject.GetComponentsInChildren<SpriteRenderer> () [0].sprite = towerImages [(int)towerID];
		newObject.GetComponent<Item> ().towerID = (int)towerID;
		Table.GetComponent<UITable>().Reposition();

	}

	public void AddTower(int a){
		gameControl.towerResource [a]++;
		if (gameControl.towerResource [a] == 1) {
			AddButton (a);
		} else {
			for (int i = 0; i < Table.transform.childCount; i++)
			{
				Transform child = Table.transform.GetChild(i);
				if (child.GetComponent<Item>().towerID == a){
					child.GetComponent<Item>().LabelCount(gameControl.towerResource [a]);
					break;
				}
			}

		}

		refreshTip ();

	}
	IEnumerator GainTowerResource(){
		while (towerResourceCheck){
			int a = (int)Random.Range (0, 6);
			circleGauge.GetComponentInChildren<UIFilledSprite> ().fillAmount = 1;
			circleGauge.GetComponentsInChildren<SpriteRenderer> () [0].sprite = towerImages [a];
			yield return new WaitForSeconds(ResourceTime);
			AddTower(a);
		}
	}
	public static int GetPlayerLife(){
		return gameControl.playerLife;
	}


	void OnWaveStartSpawned(int waveID){
		currentWave+=1;
		
		//if game is not yet started, start it now
		if(gameState==_GameState.Idle) gameState=_GameState.Started;
	}
	static public void GainResource(int val){
		gameControl.playerResource+=val;
		if(onResourceE!=null) onResourceE();
	}
	
	static public void GainResource(int[] val){
		gameControl.playerResource+=val[0];
		if(onResourceE!=null) onResourceE();
	}
	
	static public void SpendResource(int val){
		gameControl.playerResource-=val;
		if(onResourceE!=null) onResourceE();
	}
	
	static public void SpendResource(int[] val){
		gameControl.playerResource-=val[0];
		if(onResourceE!=null) onResourceE();
	}
	static public int GetResourceVal(){
		return gameControl.playerResource;
	}
	
	static public bool HaveSufficientResource(int val){
		if(gameControl.playerResource>=val) return true;
		return false;
	}
	
	static public bool HaveSufficientResource(int[] vals){
		if(gameControl.playerResource>=vals[0]) return true;
		return false;
	}

	/* Mix Info */
	public GameObject[] MixItems;
	public GameObject[] EqualLabel;
	public GameObject backButton;
	public GameObject[] MixButton;

	private int[,] MixTable = new int[100, 4];
	private int defaultClass;

	private int[] stackID = new int[1000];
	private int stackCnt = 0;
	private int currentTowerID;

	void MixTableInit(){
		int i, j;
		for (i=0; i<100; i++){
			for (j=0; j<4; j++){
				MixTable[i,j] = -1;
			}
		}


		defaultClass = 1;
		stackCnt = 0;

		MixTable [2,0] = 0;
		MixTable [2,1] = 0;

		MixTable [3,0] = 1;
		MixTable [3,1] = 1;

		MixTable [4,0] = 1;
		MixTable [4,1] = 2;
		MixTable [4,2] = 2;

		MixTable [5,0] = 1;
		MixTable [5,1] = 1;
		MixTable [5,2] = 1;
		MixTable [5,3] = 3;

		currentTowerID = -1;

		clearTable ();
	}

	void clearTable(){
		int i;
		for (i=0; i<20; i++){
			MixItems[i].SetActive(false);
		}
		for (i=0; i<4; i++){
			EqualLabel[i].SetActive(false);
		}
		backButton.SetActive (false);
		for (i=0; i<3; i++){
			MixButton[i].SetActive (false);
		}
	}
	int findInBuildTowerResource(int towerID){
		for (int i=0; i<buildTowerResource.Length; i++){
			if (buildTowerResource[i]==towerID) return i;
		}
		return -1;
	}

	void MixSetSprite(Sprite img, GameObject item, int towerID, bool black){
		item.SetActive (true);
		item.GetComponentInChildren<SpriteRenderer> ().sprite = img;
		if (black)
			item.GetComponentInChildren<SpriteRenderer> ().color = Color.black;
		else
			item.GetComponentInChildren<SpriteRenderer> ().color = Color.white;
				
		item.GetComponent<MixItem>().towerID = towerID;
	
	}
	/*

	public bool CanMix(int towerID){
		int[] temp = (int[])towerResource.Clone ();
		for (int i =0; i<buildTowerResource.Length; i++){
			if (buildTowerResource[i] != -1) temp[buildTowerResource[i]]++;
		}
		Debug.Log ("mmmmmmmmmmmmmmmmmmm");
		for (int i =0; i<4; i++){
			Debug.Log ("haha : " + MixTable[towerID, i].ToString ());
			if (MixTable[towerID,i] != -1){
				temp[MixTable[towerID, i]]--;
				if (temp[MixTable[towerID, i]] < 0) return false;
			}
		}
		return true;
	}
*/

	public void showTip(int TowerID, int pushToStack){  // pushToStack : 0 -> no, 1 : yes, 2 : reset

		if (currentTowerID == TowerID) {
			pushToStack = 0;
		}

		clearTable ();

		if (pushToStack == 2) stackCnt = 0;
		if (pushToStack ==1 && currentTowerID != -1) stackID[stackCnt++] = currentTowerID;
		currentTowerID = TowerID;
		if (stackCnt > 0) backButton.SetActive (true);

		//Debug.Log(TowerID);
		MixSetSprite(towerImages[TowerID], MixItems[0], TowerID, false);

		int x = 1;
		int y = 0;
		if (TowerID > defaultClass) {

			EqualLabel[0].SetActive (true);
			for (int i=0; i<4; i++){
				if (MixTable[TowerID, i] != -1){
					MixSetSprite(towerImages[MixTable[TowerID, i]], MixItems[x++], MixTable[TowerID, i], false);
				}
			}
		}

		x = 0;
		y++;

		for (int k=0; k<100; k++){
			int j;
			for (j=0; j<4; j++){
				if (MixTable[k,j] == TowerID) break; 
			}
			if (j!=4){
				EqualLabel[y].SetActive (true);
				MixSetSprite(towerImages[k], MixItems[y*5+x], k, false);
				x++;

				bool canMix = true;

				int[] temp = (int[])towerResource.Clone ();
				for (int i =0; i<buildTowerResource.Length; i++){
					if (buildTowerResource[i] != -1) temp[buildTowerResource[i]]++;
				}
				for (int i =0; i<4; i++){
					if (MixTable[k,i] != -1){
						temp[MixTable[k, i]]--;
						if (temp[MixTable[k, i]] < 0){
							canMix = false;
							MixSetSprite(towerImages[MixTable[k, i]], MixItems[y*5+x], MixTable[k, i], true);
							x++;
						}else{
							MixSetSprite(towerImages[MixTable[k, i]], MixItems[y*5+x], MixTable[k, i], false);
							x++;
						}
					}
				}
				x = 0;
				if (canMix) MixButton[y-1].SetActive(true);
				y++;
			}
		}
	}
	public void RemoveTowerFromBag(int towerID){
		//gameControl.towerResource [towerID]--;
		for (int i = 0; i < Table.transform.childCount; i++)
		{
			Transform child = Table.transform.GetChild(i);
			if (child.GetComponent<Item>().towerID == towerID){
				RemoveTower(child.GetComponent<Item>());
				break;
			}
		}
	}

	public void RemoveTowerFromPlatforms(int towerID){
		int platformID = findInBuildTowerResource (towerID);
		if (platformID != -1) buildTowerResource [platformID] = -1;
		//BuildManager.buildManager.platforms [platformID].GetComponent<Platform> ().available = true;
		BuildManager.buildManager.platforms [platformID].GetComponent<Platform> ().tower.Destroy();
	}
	public void backTip(){
		stackCnt--;
		showTip (stackID [stackCnt], 0);
	}
	public void refreshTip(){
		if (currentTowerID == -1) return;
		showTip (currentTowerID, 0);
	}


	public void Mix(int buttoni){
		int i, j;
		int k=0;

		for (i=0; i<100; i++){
			for (j=0; j<4; j++){
				if (MixTable[i,j] == currentTowerID) break; 
			}
			if (j!=4){
				if (k==buttoni){
					for (j=0; j<4; j++){
						if (MixTable[i,j] != -1){
							if (towerResource[MixTable[i, j]] > 0) RemoveTowerFromBag (MixTable[i, j]);
							else{
								RemoveTowerFromPlatforms(MixTable[i, j]);
							}
						}
					}
					//Debug.Log (i);
					AddTower(i);
					refreshTip();
					return;
				}
				k++;
			}
		}
	}



	/* item */
	public GameObject coin;

	void ItemInit(){
		coin.layer = LayerManager.LayerItem ();
		ObjectPoolManager.New (coin, 30);
	}
	public void GainItem(Transform unit, int val){
		GameObject obj = ObjectPoolManager.Spawn (coin, unit.position, unit.rotation);
		//StartCoroutine(CoinClick (val));
		StartCoroutine (CoinDestroy (obj, val));
	}

	IEnumerator CoinDestroy(GameObject coin, int val){
		//coin.GetComponent<
		yield return new WaitForSeconds(3f);

		GainResource(val);
		while(coin.GetComponent<SpriteRenderer>().color.a > 0){
			Color temp = coin.GetComponent<SpriteRenderer>().color;
			coin.GetComponent<SpriteRenderer>().color = new Color(temp.r, temp.g, temp.b, temp.a - 0.10f);
			Vector3 temp2 = coin.transform.position;
			coin.transform.position = new Vector3(temp2.x, temp2.y+0.2f, temp2.z);
			yield return new WaitForSeconds(0.05f);
		}
		ObjectPoolManager.Unspawn (coin);
		yield return null;
	}


}

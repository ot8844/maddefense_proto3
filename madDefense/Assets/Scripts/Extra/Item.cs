﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {
	public int towerID;
	public UILabel cnt;

	public void ButtonDestroy(){
		UITable a = this.transform.parent.GetComponent<UITable> ();
		Destroy (this.gameObject);
		a.Reposition ();
	}

	public void LabelCount(int count){
		cnt.text = "x" + count.ToString ();
	}
	void ItemClick(){
		UnitTower[] towerList = BuildManager.GetTowerList ();
		UnitTower tower = towerList[towerID];
		BuildManager.BuildTowerDragNDrop (tower, this);
		GameControl.gameControl.showTip (towerID, 2);
	}
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LayerManager : MonoBehaviour {
	
	public static bool initiated=false;

	public static int layerCreep=31;
	public static int layerCreepF=30;
	public static int layerTower=29;
	public static int layerPlatform=28;
	public static int layerOverlay=25;
	public static int layerMiscUI=27;
	public static int creepTower=24;
	public static int layerItem = 26;
	
	public static LayerManager layerManager;
	
	void Awake(){
		layerManager=this;
		
		/*
      #if UNITY_EDITOR
      GameControl gameControl=gameObject.GetComponent<GameControl>();
      if(gameControl!=null){
         gameControl.layerManager=this;
      }
      #endif*/
	}
	
	
	
	public static void Init(){
		if(layerManager==null){
			GameObject obj=new GameObject();
			obj.name="LayerManager";
			
			layerManager=obj.AddComponent<LayerManager>();
			
			Debug.Log("init   "+layerManager);
		}
	}
	
	public static int LayerCreep(){
		return layerCreep;
		//return layerCreep;
	}
	
	public static int LayerCreepF(){
		return layerCreepF;
		//return layerCreepF;
	}
	
	public static int LayerTower(){
		return layerTower;
		//return layerTower;
	}
	
	public static int LayerPlatform(){
		return layerPlatform;
		//return layerPlatform;
	}
	
	public static int LayerOverlay(){
		return layerOverlay;
		//return layerPlatform;
	}
	
	public static int LayerMiscUIOverlay(){
		return layerMiscUI;
		//return layerPlatform;
	}
	
	public static int LayerCreepTower(){
		return creepTower;
	}

	public static int LayerItem(){
		return layerItem;
	}
}
using UnityEngine;
using System.Collections;

public class BuildManager : MonoBehaviour {

	public UnitTower[] towers;

	public Transform[] platforms;
	private Platform[] buildPlatforms;

	static private float _gridSize = 0;
	public float gridSize = 2;

	static public BuildManager buildManager;
	static private BuildableInfo currentBuildInfo;

	public static Item currentBuildButton;

	static private int towerCount = 0;

	void Awake(){
		buildManager = this;

		foreach (UnitTower tower in towers) {
			tower.thisObj = tower.gameObject;
		}

		towerCount = 0;

		InitPlatform ();
	}

	void InitPlatform() {

		buildPlatforms = new Platform[platforms.Length];

		int i = 0;
		foreach(Transform basePlane in platforms){
			Platform platform = basePlane.gameObject.GetComponent<Platform>();

			if(platform == null){
				platform = basePlane.gameObject.AddComponent<Platform>();
			}

			platform.platformID = i;
			buildPlatforms[i] = platform;

			i++;
		}
	}

	void Start(){
		BuildManager.InitiateSampleTower();
	}

	void Update() {
	}

	static public bool CheckBuildPoint(Vector3 pointer){

		BuildableInfo buildableInfo = new BuildableInfo ();

		LayerMask maskPlatform = 1 << LayerManager.LayerPlatform ();
		Ray ray = Camera.main.ScreenPointToRay (pointer);
		RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity, maskPlatform);

		if(hit.collider != null){
			for(int i=0; i<buildManager.buildPlatforms.Length; i++){

				Transform basePlane = buildManager.buildPlatforms[i].thisT;
				if(hit.transform == basePlane){
					if(!buildManager.buildPlatforms[i].available) return false;

					Vector3 pos=basePlane.position;

					buildableInfo.buildable=true;
					buildableInfo.position=pos;
					buildableInfo.platform=buildManager.buildPlatforms[i];

					break;
				}
			}
		}
		else return false;

		currentBuildInfo = buildableInfo;
		
		return true;
	}

	public void ChangePlatformSkin(){
		for (int i=0; i<buildManager.buildPlatforms.Length; i++){
			if(buildManager.buildPlatforms[i].available) buildManager.buildPlatforms[i].ChangeAvailableSkin();
			else buildManager.buildPlatforms[i].ChangeNotavailableSkin();
		}
	}

	
	public void ReturnPlatformSkin(){
		for (int i=0; i<buildManager.buildPlatforms.Length; i++){
			buildManager.buildPlatforms[i].ChangeAvailableSkin();
		}
	}	
	public static void BuildTowerDragNDrop(UnitTower tower, Item button){		
		int ID = 0;
		for (int i=0; i<buildManager.towers.Length; i++){
			if (buildManager.towers[i] == tower){
				ID = i;
				break;
			}
		}
		buildManager.ChangePlatformSkin ();
		buildManager.sampleTower [ID].thisObj.SetActiveRecursively (true);
		UnitTower towerCom = buildManager.sampleTower [ID];
		buildManager.sampleTower [ID].tower.SetActive (false);
		currentBuildButton = button;

		towerCom.StartCoroutine(towerCom.DragNDropRoutine());
	}

	public static void DragNDropBuilt(UnitTower tower){
		int ID = 0;
		for(int i=0; i<buildManager.towers.Length; i++){
			if(buildManager.sampleTower[i] == tower){
				ID = i;
				break;
			}
		}
		if(currentBuildInfo == null) return;

		/* build success */
		currentBuildInfo.platform.available = false;
		GameObject towerObj = (GameObject)Instantiate (tower.thisObj, currentBuildInfo.position, currentBuildInfo.platform.thisT.rotation);
		UnitTower towerCom = towerObj.GetComponent<UnitTower> ();
		towerCom.InitTower (ID);
		towerCom.UpdateZ ();
		towerCom.isBuilt = true;
		towerCom.buildPlatform = currentBuildInfo.platform;
		towerCom.wpCounter = currentBuildInfo.platform.platformID / 2;
		buildManager.ReturnPlatformSkin ();

		currentBuildInfo.platform.tower = towerCom;
		GameControl.RemoveTower (currentBuildButton);
		GameControl.gameControl.buildTowerResource [currentBuildInfo.platform.platformID] = ID;
		GameControl.gameControl.refreshTip ();
	
		currentBuildInfo = null;	

	}

	public static void DragNDropFail(){
		buildManager.ReturnPlatformSkin ();
		currentBuildInfo = null;
	}

	private UnitTower[] sampleTower;
	private int surrentSampleID = -1;
	public static void InitiateSampleTower(){
		buildManager.sampleTower = new UnitTower[buildManager.towers.Length];
		for(int i=0; i<buildManager.towers.Length; i++){
			GameObject towerObj= (GameObject)Instantiate (buildManager.towers[i].gameObject);
			buildManager.sampleTower[i] = towerObj.GetComponent<UnitTower>();

			towerObj.SetActiveRecursively(false);
		}
	}

	static public BuildableInfo GetBuildInfo(){
		return currentBuildInfo;
	}

	static public UnitTower[] GetTowerList(){
		return buildManager.towers;
	}

}
[System.Serializable]
public class BuildableInfo{
	public bool buildable=false;
	public Vector3 position=Vector3.zero;
	public Platform platform;
	//public GameObject[] buildableTower=null;
	
	public int[] specialBuildableID;
	
	//cant build
	public void BuildSpotInto(){}
	
	//can build anything
	public void BuildSpotInto(Vector3 pos){
		position=pos;
	}

}
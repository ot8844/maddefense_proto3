﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum _SpawnMode{Continuous}

public class SpawnManager : MonoBehaviour {
	public delegate void WaveStartSpawnHandler(int waveID);
	public static event WaveStartSpawnHandler onWaveStartSpawnE;
	
	public delegate void WaveSpawnedHandler(int waveID);
	public static event WaveSpawnedHandler onWaveSpawnedE;
	
	public delegate void WaveClearedHandler(int waveID);
	public static event WaveClearedHandler onWaveClearedE;

	public _SpawnMode spawnMode=_SpawnMode.Continuous;
	public PathTD defaultPath;
	private List<Vector3> waypoints;

	static public SpawnManager spawnManager;

	public Wave[] waves=new Wave[1];

	private int currentWave=0;
	private int totalSpawnCount=0;

	private float timeLastSpawn;
	private float waitDuration;

	void Awake(){
		spawnManager=this;
	}

	// Use this for initialization
	void Start () {
		foreach(Wave wave in waves){
			foreach(SubWave subWave in wave.subWaves){
				ObjectPoolManager.New(subWave.unit, subWave.count);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool _Spawn(){
		if(GameControl.gameState!=_GameState.Ended){
			if(currentWave>=waves.Length){
				Debug.Log("All wave has been spawned");
				return false;
			}
			else{
				if(currentWave==0){
					GameControl.gameState=_GameState.Started;
				}
				//initiate corresponding spawn routine
				if(spawnMode==_SpawnMode.Continuous){
					if(currentWave==0) StartCoroutine(ContinousTimedSpawn());
					else ContinousTimedSpawnSkip();
				}
			}
		}else{
			return false;
		}
		return true;
	}

	private void ContinousTimedSpawnSkip(){
		if(GameControl.gameState!=_GameState.Ended){
			timeLastSpawn=Time.time;
			waitDuration=waves[currentWave].waveInterval;
			SpawnWave();
		}
		else Debug.Log("The game is over");
	}

	IEnumerator ContinousTimedSpawn(){
		waitDuration=waves[currentWave].waveInterval;
		timeLastSpawn=-waitDuration;
		while(currentWave<waves.Length){
			if(Time.time-timeLastSpawn>=waitDuration){
				if (GameControl.GetPlayerLife() <= 0){
					GameControl.onGameOver(false);
					break;
				}
				timeLastSpawn=Time.time;
				waitDuration=waves[currentWave].waveInterval;
				SpawnWave();
			}
			yield return null;
		}
	}

	public void SpawnWave(){
		foreach(SubWave subWave in waves[currentWave].subWaves){
			StartCoroutine(SpawnSubwave(subWave, waves[currentWave], currentWave));
		}
		currentWave+=1;
	}
	//actual spawning routine, responsible for spawning one type of creep only
	IEnumerator SpawnSubwave(SubWave subWave, Wave parentWave, int waveID){
		yield return new WaitForSeconds(subWave.delay);
		int spawnCount=0;
		while(spawnCount<subWave.count){
			Vector3 pos;
			Quaternion rot;
			
			PathTD tempPath;
			if(subWave.path==null) tempPath=defaultPath;
			else tempPath=subWave.path;
			
			pos=tempPath.waypoints[0].position;
			rot=tempPath.waypoints[0].rotation;
			
			GameObject obj=ObjectPoolManager.Spawn(subWave.unit, pos, rot);
			UnitCreep unit=obj.GetComponent<UnitCreep>();
			
			List<Vector3> waypoints=new List<Vector3>();
			foreach(Transform pointT in tempPath.waypoints){
				waypoints.Add(pointT.position);
			}
			unit.Init(waypoints, totalSpawnCount, waveID);
			totalSpawnCount+=1;
			spawnCount+=1;
			if(spawnCount==subWave.count) break;
			yield return new WaitForSeconds(subWave.interval);
		}
		subWave.spawned=true;
	}

	static public int NewUnitID(){
		spawnManager.totalSpawnCount+=1;
		return spawnManager.totalSpawnCount-1;
	}
	
	public float _TimeNextSpawn(){
		return timeLastSpawn+waitDuration-Time.time;
	}
	static public bool Spawn(){
		return spawnManager._Spawn();
	}
	
	static public int GetCurrentWave(){
		return spawnManager.currentWave;
	}
	
	static public int GetTotalWave(){
		return spawnManager.waves.Length;
	}
	
	static public float GetTimeNextSpawn(){
		return spawnManager._TimeNextSpawn();
	}
	
	static public _SpawnMode GetSpawnMode(){
		return spawnManager.spawnMode;
	}
}

[System.Serializable]
public class SubWave{
	public GameObject unit;
	public int count;
	public float interval=1;
	public float delay;
	public PathTD path;
	[HideInInspector] public bool spawned=false;
}

[System.Serializable]
public class Wave{
	public SubWave[] subWaves=new SubWave[1];
	public float waveInterval;
	[HideInInspector] public bool spawned=false; //flag indicating weather all unit in the wave have been spawn
	[HideInInspector] public bool cleared=false; //flag indicating weather the wave has been cleared
}

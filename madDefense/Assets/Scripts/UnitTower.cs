﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitTower : Unit {
	public GameObject hitEffect;
	public bool warrior; // 근접캐릭터 체크

	[HideInInspector] public bool isBuilt;
	[HideInInspector] public Platform buildPlatform;

	private int towerID=-1;
	public void SetTowerID(int ID){ towerID=ID; }
	public int GetTowerID(){ return towerID; }

	private GameObject shootObject;
	public GameObject tower;
	
	public Sprite[] image;

	public bool isDot = false;
	public float dotTotalTime = 10f;
	public float dotDuration = 1f;

	public float damage = 2;
	public float range = 8;
	public float cooldown = 1;
	
	private Unit target;
	private LayerMask maskTarget;
	private float currentTargetDist = 0;

	private Animator towerAnimator;
	private enum State{ Idle, Attack };
	private State mState;


	public LayerMask GetTargetMask(){
		return maskTarget;
	}

	[HideInInspector] public Transform shootPoint;
	
	public override void Awake(){
		base.Awake ();

		SetSubClassInt(this);
		InitStat ();
		towerAnimator = tower.GetComponent<Animator> ();
		mState = State.Idle;
		isBuilt = false;
		buildPlatform = null;
	}
	
	public override void Start(){
		base.Start ();
		thisObj.layer = LayerManager.LayerTower ();
		ObjectPoolManager.New(shootObject, 2);
		ObjectPoolManager.New(hitEffect, 10);
	}
	
	public void InitTower(int ID){
		towerID = ID;
		maskTarget = 1 << LayerManager.LayerCreep ();
		StartCoroutine (ScanForTargetAllAround ());
		StartCoroutine (TurretRoutine ());
	}
	
	
	public IEnumerator DragNDropRoutine(){
		
		bool buildEnable = false;
		
		while(true){
			bool flag=BuildManager.CheckBuildPoint(Input.mousePosition);
			BuildableInfo currentBuildInfo = BuildManager.GetBuildInfo();
			
			if(currentBuildInfo != null){
				if(flag && !buildEnable){
					buildEnable = true;
				}
				else if(!flag && buildEnable){
					buildEnable = false;
				}
			}

			if(currentBuildInfo == null){
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				Vector2 pos = new Vector2(ray.origin.x, ray.origin.y);
				//Debug.Log (pos);
				thisT.position=pos;
			}
			else{
				thisT.position=currentBuildInfo.position;
				thisT.rotation=currentBuildInfo.platform.thisT.rotation;
			}
		
			if(Input.GetMouseButtonDown(0)){
				if(flag){
					this.tower.SetActive (true);
					if (currentBuildInfo.platform.platformID < 2){
						tower.GetComponent<SpriteRenderer>().sprite = image[0];
						wpCounter = 0;
					}else if (currentBuildInfo.platform.platformID < 4){
						tower.GetComponent<SpriteRenderer>().sprite = image[1];
						wpCounter = 1;
					}else if (currentBuildInfo.platform.platformID < 6){
						tower.GetComponent<SpriteRenderer>().sprite = image[2];
						wpCounter = 2;
					}else{
						tower.GetComponent<SpriteRenderer>().sprite = image[3];
						wpCounter = 3;
					}					
					DragNDropBuilt();
				}
				else{
					DragNDropFail();
				}
				break;
			}
			
			yield return null;
		}
	}
	public void Destroy(){
		if (buildPlatform != null)
						buildPlatform.available = true;
		GameObject.Destroy(thisObj);
	}
	
	void DragNDropBuilt(){
		BuildManager.DragNDropBuilt (this);
		thisObj.SetActiveRecursively(false);
	}

	void DragNDropFail(){
		BuildManager.DragNDropFail ();
	}

	/* change target variable */
	void ScanTarget(Vector3 pos){

		Collider2D[] cols = Physics2D.OverlapCircleAll(pos, range, maskTarget);
		if(cols.Length > 0){
			Unit currentTarget = null;
			Unit targetTemp = null;
			
			float dist = Mathf.Infinity;
			Collider2D currentCollider = cols[0];
			foreach(Collider2D col in cols){
				float currentDist = Vector3.Distance(pos, col.transform.position);
				if(currentDist<dist){
					currentCollider = col;
					dist = currentDist;
				}
			}
			currentTarget = currentCollider.gameObject.GetComponent<Unit>();
			if(currentTarget!=null && currentTarget.HPAttribute.HP>0) target = currentTarget;
		}
	}

	IEnumerator ScanForTargetAllAround(){
		while (true) {
			Transform[] paths = SpawnManager.spawnManager.defaultPath.GetPath();
			Vector3 alpha = (paths[wpCounter].position - paths[(wpCounter+1)%4].position)/4;
			Vector3 pos = thisT.position + alpha;

			if(target == null){
				/*Collider2D[] cols = Physics2D.OverlapCircleAll(pos, range, maskTarget);
				if(cols.Length > 0){
					Unit currentTarget = null;
					Unit targetTemp = null;
					
					float dist = Mathf.Infinity;
					Collider2D currentCollider = cols[0];
					foreach(Collider2D col in cols){
						float currentDist = Vector3.Distance(pos, col.transform.position);
						if(currentDist<dist){
							currentCollider = col;
							dist = currentDist;
						}
					}
					currentTarget = currentCollider.gameObject.GetComponent<Unit>();
					if(currentTarget!=null && currentTarget.HPAttribute.HP>0) target = currentTarget;
				}*/
				ScanTarget(pos);
			}
			else{
				currentTargetDist=Vector3.Distance(thisT.position, target.thisT.position);
				if(currentTargetDist>range || target.HPAttribute.HP<=0 || !target.thisObj.active){
					target=null;
				}
			}
			yield return null;
		}
	}
	
	IEnumerator TurretRoutine(){
		while (true) {
			if(target!=null && Vector3.Distance(thisT.position, target.thisT.position)<range){

				/*Vector3 srcPos = thisT.position;

				tower.GetComponent<SpriteRenderer>().sprite = image[(target.wpCounter+3)%image.Length];
				GameObject obj = ObjectPoolManager.Spawn(shootObject, shootPoint.position, shootPoint.rotation);
				ShootObject shootObj = obj.GetComponent<ShootObject>();
				obj.SetActive(true);
				shootObj.Shoot(target, this, shootPoint);*/
				if (mState == State.Idle){
					mState = State.Attack;
					towerAnimator.enabled = true;
					tower.GetComponent<SpriteRenderer>().sprite = image[(target.wpCounter+3)%image.Length];
					if (warrior){
						towerAnimator.SetInteger("direction",0);
					}
					else{		
						towerAnimator.SetInteger("direction",(target.wpCounter+3)%4);
					}

				}
				yield return new WaitForSeconds(Mathf.Max (0.05f, cooldown));
			}
			else{
				yield return null;
			}
		}
		//yield return new WaitForSeconds(1);
	}
	

	private void InitStat(){
		GameObject tempObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		tempObj.transform.localScale = new Vector3 (0.3f, 0.3f, 0.3f);
		tempObj.AddComponent<ShootObject>();
		tempObj.active=false;
		
		shootObject=tempObj;
		shootPoint = thisT;
	}

	IEnumerator HitTargetColor(UnitCreep tgt, Color color, float duration){
		Color temp = tgt.creep.GetComponent<SpriteRenderer> ().color; 
		tgt.creep.GetComponent<SpriteRenderer>().color = color;
		yield return new WaitForSeconds(duration);
		tgt.creep.GetComponent<SpriteRenderer>().color = temp;
		yield return null;
	
	}
	IEnumerator ApplyDot(UnitCreep tgt){
		float time = 0;
		//StartCoroutine(HitTargetColor(tgt, new Color(162.0f/255.0f, 190.0f/255.0f, 95.0f/255.0f, 1f), dotTotalTime));
		while(time < dotTotalTime){
			StartCoroutine(HitTargetColor(tgt, new Color(162.0f/255.0f, 190.0f/255.0f, 95.0f/255.0f, 1f), dotDuration));
			GameObject a = ObjectPoolManager.Spawn(hitEffect, tgt.gameObject.transform.position + new Vector3(0, 0, -1), hitEffect.transform.rotation); 
			a.transform.parent = tgt.creep.transform;
			tgt.ApplyDamage(damage);
			yield return new WaitForSeconds(dotDuration);
			time+=dotDuration;
		}
	}
	public void HitTarget(Vector3 pos, Unit tgt){
		if(tgt.gameObject!=null && tgt.gameObject.active){
			if (tgt.subClass == _UnitSubClass.Creep) {
				if(damage>0){

					if (isDot){
						StartCoroutine(ApplyDot (tgt.unitC));
					}else{
						GameObject a = ObjectPoolManager.Spawn(hitEffect, tgt.gameObject.transform.position + new Vector3(0, 0, -1), hitEffect.transform.rotation); 
						a.transform.parent = tgt.unitC.creep.transform;
						StartCoroutine(HitTargetColor(tgt.unitC, new Color(1F, 125.0f/255.0F, 125.0f/255.0F, 1F), 0.3f));
						tgt.ApplyDamage(damage);
					}
				}
			}
		}
	}

	public void TurretAttack(){
		if (target==null) ScanTarget(thisT.position);
		if(target!=null && Vector3.Distance(thisT.position, target.thisT.position)<range && mState==State.Attack){
			if (warrior){
				HitTarget(Vector3.zero, target);
			}else{
				Vector3 srcPos = thisT.position;
				
				//tower.GetComponent<SpriteRenderer>().sprite = image[(target.wpCounter+3)%image.Length];
				
				GameObject obj = ObjectPoolManager.Spawn(shootObject, shootPoint.position, shootPoint.rotation);
				ShootObject shootObj = obj.GetComponent<ShootObject>();
				
				obj.SetActive(true);
				shootObj.Shoot(target, this, shootPoint);
			}
		}
		mState = State.Idle;
		towerAnimator.enabled = false;
	}

	
}
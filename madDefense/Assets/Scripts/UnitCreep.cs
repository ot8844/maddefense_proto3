﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class UnitCreep : Unit {
	
	public delegate void LifeInHandler(int waveID);
	public static event LifeInHandler onLifeInE;
	
	public delegate void LifeDeHandler(int waveID);
	public static event LifeDeHandler onLifeDeE;
	
	public GameObject creep;

	private Animator creepAnimator;
	//public Sprite[] image;
	
	public float moveSpeed=3;
	public int value;
	
	private int unitID;
	[HideInInspector] public int waveID;
	public override void Awake () 
	{
		base.Awake ();
		SetSubClassInt(this);
		creepAnimator = creep.GetComponent<Animator> ();
	}
	public override void Start () {
		thisObj.layer=LayerManager.LayerCreep();
		base.Start();
		currentMoveSpd=moveSpeed;
		
	}
	
	public void SetMoveSpeed(float moveSpd){
		moveSpeed=moveSpd;
	}
	
	public void Init(List<Vector3> waypoints, int uID, int wID){
		wpCounter = 0;
		
		base.Init ();
		unitID = uID;
		waveID = wID;
		wp=waypoints;
		currentMoveSpd=moveSpeed;
		
		if(onLifeDeE!=null) onLifeDeE(waveID);
	}
	
	public override void Update () {
		base.Update();
		MoveWPMode();
	}
	
	
	void MoveWPMode(){
		//execute as long as there are unreached waypoint in the path
		if(wpCounter<wp.Count){
			//move to the next waypoint, if return true, then update to the next waypoint
			if(MoveToPoint(wp[wpCounter])){
				wpCounter+=1;
				creepAnimator.SetInteger("direction",(wpCounter+3)%4);

				// 0 : LF->RF, 1 : RF->RB, 2 : RB->LB, 3 : LB->LF
			}
		}
		else {
			wpCounter = 0;
		}
	}
	
	bool MoveToPoint(Vector3 point){
		float dist=Vector2.Distance(point, thisT.position);

		Vector3 temp1 = new Vector3 (point.x, point.y, 0);
		Vector3 temp2 = new Vector3 (thisT.position.x, thisT.position.y, 0);
		if(dist<0.15f) return true;
		Vector3 dir=(temp1-temp2).normalized;
		this.UpdateZ ();
		thisT.Translate(dir*Mathf.Min(dist, currentMoveSpd * Time.deltaTime), Space.World);

		return false;
	}
	
	public void Dead(){
		//Debug.Log ("dead");
		//GameControl.GainResource(value);
		GameControl.gameControl.GainItem(thisT, value);
		StartCoroutine(Unspawn(0.05f));
		if(onLifeInE!=null) onLifeInE(waveID);
	}
	IEnumerator Unspawn(float duration){
		currentMoveSpd = 0;
		while(creep.GetComponent<SpriteRenderer>().color.a > 0){
			Color temp = creep.GetComponent<SpriteRenderer>().color;
			creep.GetComponent<SpriteRenderer>().color = new Color(temp.r, temp.g, temp.b, temp.a - 0.30f);
			yield return new WaitForSeconds(duration);
		}
		ObjectPoolManager.Unspawn(thisObj);
		yield return null;
	}
	
}


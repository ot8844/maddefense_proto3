﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Platform : MonoBehaviour {
	[HideInInspector] public GameObject thisObj;
	[HideInInspector] public Transform thisT;
	[HideInInspector] public bool available;
	public int platformID;
	public UnitTower tower;
	
	private SpriteRenderer sprite;
	public Sprite availableSprite;
	public Sprite notavailableSprite;
	//static private Object availableSprite;
	//static private Object notavailableSprite;

	void Awake(){
		thisObj = gameObject;
		thisT = transform;
		available = true;
		tower = null;
		sprite = thisObj.GetComponent<SpriteRenderer> ();
		//availableSprite = Resources.Load ("platform.png");
		//notavailableSprite = Resources.Load ("platform_notavailable.png");

		thisObj.layer = LayerManager.LayerPlatform ();
	}

	public void ChangeAvailableSkin(){
		sprite.sprite = availableSprite;
	}

	public void ChangeNotavailableSkin(){
		sprite.sprite = notavailableSprite;
	}
}
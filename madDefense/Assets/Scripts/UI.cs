﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;


//using System.Collections.Generic;

public class UI : MonoBehaviour {
	public Camera MainCamera;
	public Camera NGUICamera;

	private bool towerInfoShow = false;
	public GameObject curInfo;
	private GameObject curTower;
	private bool clickEnable;

	public static UI ui;
	private bool enableSpawnButton=true;
	private int w;
	private int h;
	public static bool winLostFlag = false;


	public UILabel MoneyCount;
	public UILabel WaveCount;
	public UILabel LifeCount;
	
	
	void Awake(){
		ui=this;
		Init ();
	}
	void OnEnable(){
	}
	void OnDisable(){
	}
	
	
	
	void Init(){
		winLostFlag = false;
		towerInfoShow = false;
		clickEnable = true;
	}
	void Start () 
	{
		w = Screen.width;
		h = Screen.height;
	}

	//caleed when SpawnManager clearFor Spawing event is detected, enable spawnButton
	void OnClearForSpawning(bool flag){
		enableSpawnButton=flag;
	}
	void BuildMenuAllTowersFix(){
		UnitTower[] towerList = BuildManager.GetTowerList ();
		
		int width = 50;
		int height = 50;
		
		int x = 0;
		int y = Screen.height - height - 6;
		
		for (int i=0; i<towerList.Length; i++){
			UnitTower tower = towerList[i];
			
			GUIContent guiContent = new GUIContent(i.ToString());
			if(GUI.Button (new Rect(x, y, width, height), guiContent)){
				BuildManager.BuildTowerDragNDrop(tower, null);
			}
			
			x += width+3;
		}
	}
	public void bagClick(){
		curInfo.transform.localScale = Vector3.zero;
		towerInfoShow = false;
		GameControl.gameControl.AddTower (curTower.GetComponent<UnitTower> ().GetTowerID ());
		curTower.GetComponent<UnitTower>().Destroy();
	}

	void SpawnClick(){
		if(enableSpawnButton){
			if(GameControl.gameState!=_GameState.Ended){
				//if spawn is successful, disable the spawnButton
				if(SpawnManager.Spawn())
					enableSpawnButton=false;
			}
		}
	}

	IEnumerator showInfo(Transform info){
		clickEnable = false;
		while (info.transform.localScale.x <30) {
			info.transform.localScale += new Vector3(0.2f*30, 0.2f*30, 1f);
			yield return new WaitForSeconds(0.05f);
		}
		//yield return new WaitForSeconds(0.5f);
		towerInfoShow = true;
		clickEnable = true;
		yield return null;
	}




	void OnGUI(){

		
		int buttonX=8;
		
		/*
		if(enableSpawnButton){
			if(GUI.Button(new Rect(8, 5, w/10, h/10 - 8), "Spawn")){
				//if the game is not ended
				if(GameControl.gameState!=_GameState.Ended){
					//if spawn is successful, disable the spawnButton
					if(SpawnManager.Spawn())
						enableSpawnButton=false;
				}
			}
			buttonX+=w/10 + 5;
		}
		*/
		int resource=GameControl.GetResourceVal();
		MoneyCount.text = resource.ToString();
		WaveCount.text = SpawnManager.GetCurrentWave () + "/" + SpawnManager.GetTotalWave ();
		LifeCount.text = "" + GameControl.GetPlayerLife ();
		
		LayerMask maskTower = 1 << LayerManager.LayerTower ();

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity, maskTower);

		if (Input.GetMouseButtonDown(0) && clickEnable){
			if (towerInfoShow){
				if (UICamera.hoveredObject ==null){
					curInfo.transform.localScale = Vector3.zero;
					towerInfoShow = false;
				}else{
					if (UICamera.hoveredObject.transform.parent != null){
						if (!UICamera.hoveredObject.transform.parent.name.Equals("info")){
							curInfo.transform.localScale = Vector3.zero;
							towerInfoShow = false;
						}
					}
				}
			}else{
				if (hit.collider != null) {
					curInfo.transform.localScale = Vector3.zero;
					curTower=hit.collider.transform.gameObject;
					if (curTower != null && curTower.GetComponent<UnitTower>().isBuilt){
						//curTower.transform.Find("info").transform.localScale = Vector3.one;
						Vector3 temp = curTower.transform.TransformPoint(curTower.GetComponent<UnitTower>().tower.transform.localPosition);
						temp = Camera.main.WorldToScreenPoint(temp) - new Vector3(Screen.width/2, Screen.height/2, 0);
						temp = new Vector3(temp.x*720.0f/Screen.height, temp.y*720.0f/Screen.height, 0);
						temp.z = 1;
						curInfo.transform.localPosition = temp;
						GameControl.gameControl.showTip(curTower.GetComponent<UnitTower>().GetTowerID (), 1);
						StartCoroutine (showInfo(curInfo.transform));

					}
				}
			}
		}
		//BuildMenuAllTowersFix ();

		if (GameControl.gameState != _GameState.Ended) {
		} else {
		}
	}
}
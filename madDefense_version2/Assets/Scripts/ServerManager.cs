using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;


public class ServerManager : MonoBehaviour {
	public static ServerManager serverManager;
	public static string serverURL = "http://bit.sparcs.org:20001/";

	public static string cid = null;
	public static string unit = null;
	public static string money = null;
	public WWW login(string id, string password){
		WWW www = new WWW (serverURL+"login/"+id+"/"+password);
		StartCoroutine(Wait_login(www, id));
		return www;
	}
	private IEnumerator Wait_login(WWW www, string id){
		yield return www;
		if (www.error == null){
			//Debug.Log ("WWW Ok!: " + www.text);
			if (www.text.Equals(id)){
				cid = id;
				Application.LoadLevel ("LoginScene2");
			}else{
				Debug.Log ("ID ERROR OR PASSWORD ERROR!");
			}
		}else{
			Debug.Log ("WWW Error: " + www.error);
		}
	}
	public WWW unitInfo(string id){
		WWW www = new WWW (serverURL+"unitInfo/"+id);
		StartCoroutine(_unitInfo(www, id));
		return www;
	}
	private IEnumerator _unitInfo(WWW www, string id){
		yield return www;
		if (www.error == null){
			//Debug.Log ("WWW Ok!: " + www.text);
			unit = www.text;
		}else{
			Debug.Log ("WWW Error: " + www.error);
		}
	}
	public WWW moneyInfo(string id){
		WWW www = new WWW (serverURL+"moneyInfo/"+id);
		StartCoroutine(_moneyInfo(www, id));
		return www;
	}
	private IEnumerator _moneyInfo(WWW www, string id){
		yield return www;
		if (www.error == null){
			//Debug.Log ("WWW Ok!: " + www.text);
			money = www.text;
		}else{
			Debug.Log ("WWW Error: " + www.error);
		}
	}
	// Use this for initialization
	void Start () {
		serverManager = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

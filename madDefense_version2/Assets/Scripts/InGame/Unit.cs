using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class UnitAttribute{
	public float fullHP = 10;
	[HideInInspector] public float HP=10;

	public Transform overlayHP;
	public Transform overlayBase;

	public bool alwaysShowOverlay=false;
	private bool overlayIsVisible=false;

	private float scaleModifierH=1;

	private Vector3 offsetH;

	private Vector3 overlayScaleH;
	private Vector3 overlayPosH;
	private Renderer overlayRendererH;


	public void Init(Transform transform){
		fullHP=Mathf.Max(0, fullHP);
		HP=fullHP;

		if(overlayHP) scaleModifierH=UnitUtility.GetWorldScale(overlayHP).x*5;
		
		if(overlayHP!=null) {
			overlayHP.gameObject.layer=LayerManager.LayerOverlay();
			overlayRendererH=overlayHP.renderer;
			overlayHP.parent=overlayBase;
			overlayScaleH=overlayHP.localScale;
			offsetH=overlayHP.localPosition;
			if(alwaysShowOverlay) overlayRendererH.enabled=true;
		}
		
		if(alwaysShowOverlay){
			overlayIsVisible=true;
		}
		
	}
	public void Reset(){
		HP=fullHP;
		UpdateOverlay();
	}
	public void GainHP(float val){
		HP=Mathf.Min(fullHP, HP+=val);
		UpdateOverlay();
	}
	public void ApplyDamage(float dmg){
		HP-=dmg;
		HP=Mathf.Clamp(HP, 0, fullHP);
		UpdateOverlay();
	}
	public void UpdateOverlay(){
		if(!overlayHP) return;

		if(!alwaysShowOverlay){
			if(overlayHP){
				if(HP>=fullHP) overlayRendererH.enabled=false;
				else if(HP<=0) overlayRendererH.enabled=false;
				else overlayRendererH.enabled=true;
			}
			
			if(overlayBase){
				if(overlayHP && overlayRendererH.enabled){
					overlayBase.renderer.enabled=true;
				}
				else{
					if(HP>=fullHP) overlayBase.renderer.enabled=false;
					else if(HP<=0) overlayBase.renderer.enabled=false;
				}
			}
			if(overlayHP && overlayRendererH.enabled){
				overlayIsVisible=true;
			}
			else{
				overlayIsVisible=false;
			}
		}
		if(overlayIsVisible){
			if(overlayHP || overlayBase){
				Vector3 dirRight=overlayBase.TransformDirection(-Vector3.right);
				if(overlayHP){
					overlayHP.localPosition=offsetH;
					float dist=((fullHP-HP)/fullHP)*scaleModifierH;
					overlayHP.Translate(dirRight*dist, Space.World);
				}
			}
		}
		if(overlayHP && overlayRendererH.enabled){
			Vector3 scale=new Vector3(HP/fullHP*overlayScaleH.x, overlayScaleH.y, overlayScaleH.z);
			overlayHP.localScale=scale;
		}
		if(overlayBase) if(HP<=0) overlayBase.renderer.enabled=false;
	}
}






public class Unit : MonoBehaviour {
	protected bool dead=false;
	protected float currentMoveSpd;
	public UnitAttribute HPAttribute;
	[HideInInspector] public Transform thisT;
	[HideInInspector] public GameObject thisObj;

	public int wpCounter=0;
	protected List<Vector3> wp=new List<Vector3>();

	[HideInInspector] public enum _UnitSubClass{None, Creep, Tower};
	[HideInInspector] public _UnitSubClass subClass;
	[HideInInspector] public UnitCreep unitC;
	[HideInInspector] public UnitTower unitT;
	public void SetSubClassInt(UnitCreep unit){ 
		unitC=unit; 
		subClass=_UnitSubClass.Creep;
	}

	public void SetSubClassInt(UnitTower unit){ 
		unitT=unit; 
		subClass=_UnitSubClass.Tower;
	}


	public virtual void Awake(){
		thisT=transform;
		thisObj=gameObject;
		subClass = _UnitSubClass.None;
		HPAttribute.Init(thisT);
	}

	// Use this for initialization
	public virtual void Start () {
		Init();
		//StartCoroutine (TestOverlay ());
	}

	public virtual void Init(){
		HPAttribute.Reset();

		dead=false;
	}
	
	// Update is called once per frame
	public virtual void Update () {

	}

	public void UpdateZ(){
		Vector3 pos = thisT.position;
		float temp = pos.y + 100f;
		pos = new Vector3(pos.x, pos.y, temp);
		thisT.position = pos;
	}

	public void SetFullHP(float hp){
		HPAttribute.fullHP=hp;
		HPAttribute.HP=HPAttribute.fullHP;
	}
	public void GainHP(float val){
		HPAttribute.GainHP(val);
	}
	IEnumerator TestOverlay(){
		yield return new WaitForSeconds(0.75f);
		while(true){
			ApplyDamage(0.1f*HPAttribute.fullHP*0.1f);
			yield return new WaitForSeconds(0.1f);
		}
	}
	public void ApplyDamage(float dmg){
		ApplyDamage(dmg, 0);
	}
	public void ApplyDamage(float dmg, int dmgType){
		HPAttribute.ApplyDamage(dmg);
		
		//if(subClass==_UnitSubClass.Creep && !dead) unitC.PlayHit();
		if(HPAttribute.HP<=0 && !dead){
			HPAttribute.HP=0;
			dead=true;
			if(subClass==_UnitSubClass.Creep){
				unitC.Dead();
			}
			else{
				ObjectPoolManager.Unspawn(thisObj);
			}
		}
	}




}

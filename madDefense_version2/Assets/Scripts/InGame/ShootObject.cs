using UnityEngine;
using System.Collections;

public class ShootObject : MonoBehaviour {
	public Unit target;
	private UnitTower srcTower;
	private Transform shootPoint;

	private GameObject thisObj;
	private Transform thisT;

	private bool hit;

	void Awake(){
		thisObj = gameObject;
		thisT = transform;
	}

	void Start(){
	}

	public void Shoot(Unit tgt, UnitTower src, Transform sp){
		target = tgt;
		srcTower = src;
		shootPoint = sp;

		hit = false;

		_Shoot ();
	}

	public void _Shoot(){
		StartCoroutine (BulletRoutine ());
	}

	public float speed = 30;

	IEnumerator BulletRoutine(){
		Vector3 targetPos=target.thisT.position;
		
		//make sure the shootObject is facing the target and adjust the projectile angle
		thisT.LookAt(target.thisT);
		
		Vector3 startPos=thisT.position;
		float iniRotX=thisT.rotation.eulerAngles.x;

		//while the shootObject havent hit the target
		while(!hit){

			if(target!=null && target.gameObject.active){
				targetPos=target.thisT.position;
			}

			float currentDist=Vector3.Distance(thisT.position, targetPos);
			if(currentDist<0.25 && !hit) Hit();

			Quaternion wantedRotation=Quaternion.LookRotation(targetPos-thisT.position);

			thisT.rotation=Quaternion.Euler(wantedRotation.eulerAngles.x, wantedRotation.eulerAngles.y, wantedRotation.eulerAngles.z);

			thisT.Translate(Vector3.forward*Mathf.Min(speed*Time.deltaTime, currentDist));
			
			yield return null;
		}
	}

	public void Hit(){
		hit = true;

		if(srcTower) srcTower.HitTarget(thisT.position, target);
		
		Unspawn();
	}

	public void Unspawn(){
		ObjectPoolManager.Unspawn(thisObj);
	}
}
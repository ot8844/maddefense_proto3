﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {
	
	static private AudioObject[] audioObject;
	
	static public AudioManager audioManager;
	
	static private Transform cam;
	
	public float minFallOffRange=10;
	
	public AudioClip[] musicList;
	public bool playMusic=true;
	public bool shuffle=false;
	private int currentTrackID=0;
	private AudioSource musicSource;

	public AudioClip backgroundSound;
	public AudioClip playClickSound;
	public AudioClip buildTowerSound;
	public AudioClip towerClickSound;
	public AudioClip MoneySound;
	public AudioClip HitSound;
	public AudioClip HitSound2;
	public AudioClip playHealSound;
	public AudioClip playWarningSound;
	public static AudioObject warningObject;


	private GameObject thisObj;

	static public void PlayHitSound2(){
		if(audioManager.HitSound2!=null) PlaySound(audioManager.HitSound2, false, 1f);
	}
	static public void PlayHitSound(){
		if(audioManager.HitSound!=null) PlaySound(audioManager.HitSound, false, 1f);
	}

	static public void PlayMoneySound(){
		if(audioManager.MoneySound!=null) PlaySound(audioManager.MoneySound, false, 1f);
	}

	static public void PlayBuildTower(){
		if(audioManager.buildTowerSound!=null) PlaySound(audioManager.buildTowerSound, false, 1f);
	}
	static public void PlayTowerClick(){
		if(audioManager.towerClickSound!=null) PlaySound(audioManager.towerClickSound, false, 1f);
	}

	static public void PlayBackground(){
		if(audioManager.backgroundSound!=null) PlaySound(audioManager.backgroundSound, true, 0.3f);
	}
	static public void PlayPlayClickSound(){
		if(audioManager.playClickSound!=null) PlaySound(audioManager.playClickSound, false, 1f);
	}

	static public void PlayHeal(){
		if(audioManager.playHealSound!=null) PlaySound(audioManager.playHealSound, false, 0.5f);
	}


	static public void MusicStop(){
		foreach (AudioObject a in audioObject){
			if (a != null){
				a.source.Stop ();
				a.inUse = false;
			}
		}
		
	}


	static public void warningSoundStop(){
		if (warningObject != null){
			warningObject.source.Stop ();
			warningObject.inUse = false;
			warningObject = null;
		}

	}
	private IEnumerator ClearWarningAudioObject(int ID, float duration){
		yield return new WaitForSeconds(duration);
		audioObject[ID].inUse=false;
	}

	static public void PlayWarningSound(AudioClip clip, bool loop, float volume){
		if(audioManager==null) Init();
		
		int ID=GetUnusedAudioObject();
		warningObject = audioObject [ID];
		audioObject[ID].inUse=true;
		audioObject [ID].source.loop = loop;
		audioObject[ID].source.clip=clip;
		audioObject[ID].source.volume=volume;
		audioObject[ID].source.Play();
		
		float duration=audioObject[ID].source.clip.length;

	}

	static public void PlayWarning(){
		if(audioManager.playWarningSound!=null && warningObject == null){
			PlayWarningSound(audioManager.playWarningSound, true, 1f);
		}
	}


	void Awake(){
		//Init();

		thisObj=gameObject;
		
		cam=Camera.main.transform;
		
		if(playMusic && musicList!=null && musicList.Length>0){
			GameObject musicObj=new GameObject();
			musicObj.name="MusicSource";
			musicObj.transform.position=cam.position;
			musicObj.transform.parent=cam;
			musicSource=musicObj.AddComponent<AudioSource>();
			musicSource.loop=false;
			musicSource.playOnAwake=false;
			
			musicSource.ignoreListenerVolume=true;
			
			StartCoroutine(MusicRoutine());
		}
		
		audioObject=new AudioObject[20];
		for(int i=0; i<audioObject.Length; i++){
			GameObject obj=new GameObject();
			obj.name="AudioSource";
			
			AudioSource src=obj.AddComponent<AudioSource>();
			src.playOnAwake=false;
			src.loop=false;
			src.minDistance=minFallOffRange;
			
			Transform t=obj.transform;
			t.parent=thisObj.transform;
			
			audioObject[i]=new AudioObject(src, t);
		}
		
		AudioListener.volume=0.8f;
		
		if(audioManager==null) audioManager=this;


		audioManager = this;
		PlayBackground ();

	}
	
	static public void Init(){
		if(audioManager==null){
			GameObject objParent=new GameObject();
			objParent.name="AudioManager";
			audioManager=objParent.AddComponent<AudioManager>();
		}		

	}
	
	public IEnumerator MusicRoutine(){
		while(true){
			if(shuffle) musicSource.clip=musicList[Random.Range(0, musicList.Length)];
			else{
				musicSource.clip=musicList[currentTrackID];
				currentTrackID+=1;
				if(currentTrackID==musicList.Length) currentTrackID=0;
			}
			
			musicSource.Play();
			
			yield return new WaitForSeconds(musicSource.clip.length);
		}
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	//check for the next free, unused audioObject
	static private int GetUnusedAudioObject(){
		for(int i=0; i<audioObject.Length; i++){
			if(!audioObject[i].inUse){
				return i;
			}
		}
		
		//if everything is used up, use item number zero
		return 0;
	}
	
	//this is a 3D sound that has to be played at a particular position following a particular event
	static public void PlaySound(AudioClip clip, Vector3 pos){
		if(audioManager==null) Init();
		
		int ID=GetUnusedAudioObject();
		
		audioObject[ID].inUse=true;
		
		audioObject[ID].thisT.position=pos;
		audioObject[ID].source.clip=clip;
		audioObject[ID].source.Play();
		
		float duration=audioObject[ID].source.clip.length;
		
		audioManager.StartCoroutine(audioManager.ClearAudioObject(ID, duration));
	}
	
	//this no position has been given, assume this is a 2D sound
	static public void PlaySound(AudioClip clip, bool loop, float volume){
		if(audioManager==null) Init();
		
		int ID=GetUnusedAudioObject();
		
		audioObject[ID].inUse=true;
		audioObject [ID].source.loop = loop;
		audioObject[ID].source.clip=clip;
		audioObject[ID].source.volume=volume;
		audioObject[ID].source.Play();
		
		float duration=audioObject[ID].source.clip.length;
		
		if (!loop) audioManager.StartCoroutine(audioManager.ClearAudioObject(ID, duration));
	}
	
	//a sound routine for 2D sound, make sure they follow the listener, which is assumed to be the main camera
	static IEnumerator SoundRoutine2D(int ID, float duration){
		while(duration>0){
			audioObject[ID].thisT.position=cam.position;
			yield return null;
		}
		
		//finish playing, clear the audioObject
		audioManager.StartCoroutine(audioManager.ClearAudioObject(ID, 0));
	}
	
	//function call to clear flag of an audioObject, indicate it's is free to be used again
	private IEnumerator ClearAudioObject(int ID, float duration){
		yield return new WaitForSeconds(duration);
		
		audioObject[ID].inUse=false;
	}
	static public void SetSFXVolume(float val){
		AudioListener.volume=val;
	}
	
	static public void SetMusicVolume(float val){
		if(audioManager  && audioManager.musicSource){
			audioManager.musicSource.volume=val;
		}
	}
	
}


[System.Serializable]
public class AudioObject{
	public AudioSource source;
	public bool inUse=false;
	public Transform thisT;
	
	public AudioObject(AudioSource src, Transform t){
		source=src;
		thisT=t;
	}
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitTower : Unit {
	public GameObject hitEffect;
	public bool warrior; // 근접캐릭터 체크

	[HideInInspector] public bool isBuilt;
	[HideInInspector] public Platform buildPlatform;

	private int towerID=-1;
	public void SetTowerID(int ID){ towerID=ID; }
	public int GetTowerID(){ return towerID; }

	private GameObject shootObject;
	public GameObject tower;
	
	public Sprite[] image;

	public bool isDot = false;
	public float dotTotalTime = 10f;
	public float dotDuration = 1f;

	public bool isSlow = false;
	public float slowTime = 5f;
	public float slowPer = 0.5f;

	public bool isSplash = false;
	public float splashRange = 2f;
	public int splashCnt = 4;

	private float sDamage;
	private float sAPDamage;
	private float sAttackSpeed;
	public float damage = 2;
	public float range = 8;
	public float cooldown = 1;
	public float APdamage = 0;
	public string towerName;

	public GameObject tornadoEffect;

	public enum TowerClass{Human, Animal, Fairy};
	public TowerClass towerClass;


	[HideInInspector]public Unit target;
	private LayerMask maskTarget;
	private float currentTargetDist = 0;

	public Animator towerAnimator;
	private enum State{ Idle, Attack };
	private State mState;

	[HideInInspector]public Vector3 thisPos;


	public LayerMask GetTargetMask(){
		return maskTarget;
	}

	[HideInInspector] public Transform shootPoint;
	
	public override void Awake(){
		base.Awake ();

		SetSubClassInt(this);
		InitStat ();
		towerAnimator = tower.GetComponent<Animator> ();
		mState = State.Idle;
		isBuilt = false;
		buildPlatform = null;
		tornadoEffect = null;
		ObjectPoolManager.New(shootObject, 2);
		if (hitEffect.activeSelf){
			ObjectPoolManager.New(hitEffect, 20);
		}
	}
	
	public override void Start(){
		base.Start ();
		thisObj.layer = LayerManager.LayerTower ();

	}
	public int getClassNumber(){
		if (towerClass == TowerClass.Human) return 0;
		if (towerClass == TowerClass.Animal) return 1;
		if (towerClass == TowerClass.Fairy) return 2;
		return -1;
	}

	public void upDamage(float delta){
		damage = sDamage+ delta;
	}
	public void upAPDamage(float delta){
		APdamage = sAPDamage + delta;
	}
	public void upAttackSpeed(float delta){
		if (sAttackSpeed-delta < 0.15f) {
			cooldown = 0.15f;
		}else{
			cooldown = sAttackSpeed - delta;
		}
	}

	public void InitTower(int ID, Platform platform){
		tower.gameObject.SetActive (true);
		sDamage = damage;
		sAPDamage = APdamage;
		sAttackSpeed = cooldown;

		UpdateZ ();
		isBuilt = true;
		buildPlatform = platform;
		wpCounter = platform.platformID / 3;
		float[] dirX = new float[4]{-1f,1f,1f, -1f };
		float[] dirY = new float[4]{-1f,-1f,1f, 1f};
		Vector3 beta = new Vector3 (1.62f*dirX[wpCounter], 1.15f*dirY[wpCounter], 0);
		thisPos = transform.position;
		if (warrior){
			thisPos+=beta;
		}
		thisObj.transform.Find ("upgrade").gameObject.SetActive (false);
		UpgradeManager.upgradeManager.upgradeShow (this);

		//Debug.Log (cooldown);

		towerID = ID;
		maskTarget = 1 << LayerManager.LayerCreep ();
		StartCoroutine (ScanForTargetAllAround ());
		StartCoroutine (TurretRoutine ());
	}
	
	
	public IEnumerator DragNDropRoutine(){
		
		bool buildEnable = false;
		
		while(true){
			bool flag=BuildManager.CheckBuildPoint(Input.mousePosition);
			BuildableInfo currentBuildInfo = BuildManager.GetBuildInfo();
			
			if(currentBuildInfo != null){
				if(flag && !buildEnable){
					buildEnable = true;
				}
				else if(!flag && buildEnable){
					buildEnable = false;
				}
			}

			if(currentBuildInfo == null){
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				Vector2 pos = new Vector2(ray.origin.x, ray.origin.y);
				//Debug.Log (pos);
				thisT.position=pos;
			}
			else{
				thisT.position=currentBuildInfo.position;
				thisT.rotation=currentBuildInfo.platform.thisT.rotation;

				//this.tower.SetActive (true);

				if (currentBuildInfo.platform.platformID < 3){
					this.tower.GetComponent<SpriteRenderer>().sprite = image[0];
					wpCounter = 0;
				}else if (currentBuildInfo.platform.platformID < 6){
					this.tower.GetComponent<SpriteRenderer>().sprite = image[1];
					wpCounter = 1;
				}else if (currentBuildInfo.platform.platformID < 9){
					this.tower.GetComponent<SpriteRenderer>().sprite = image[2];
					wpCounter = 2;
				}else{
					this.tower.GetComponent<SpriteRenderer>().sprite = image[3];
					wpCounter = 3;
				}	
			}
			
			if(Input.GetMouseButtonDown(0)){
				UI.ui.clickEnable = true;
				for (int i =0; i< BuildManager.buildManager.platforms.Length; i++){
					UnitTower tower = BuildManager.buildManager.platforms[i].gameObject.GetComponent<Platform>().tower;
					if (tower!=null){
						Color c = tower.tower.GetComponent<SpriteRenderer>().color;
						tower.tower.GetComponent<SpriteRenderer>().color = new Color(c.r, c.g, c.b, 1.0f);
					}

				}
				

				if(flag){
								
					DragNDropBuilt();
				}
				else{
					DragNDropFail();
				}
				break;
			}
			
			yield return null;
		}
	}
	public void Destroy(){
		if (buildPlatform != null){
			GameControl.gameControl.buildTowerResource[buildPlatform.platformID] = -1;
			buildPlatform.available = true;
		}
		if (tornadoEffect != null)
						ObjectPoolManager.Unspawn (tornadoEffect);
		GameObject.Destroy(thisObj);
	}
	
	void DragNDropBuilt(){
		BuildManager.DragNDropBuilt (this);
		thisObj.SetActiveRecursively(false);
	}

	void DragNDropFail(){
		BuildManager.DragNDropFail (this);
	}

	/* change target variable */
	void ScanTarget(Vector3 pos){


		Collider2D[] cols = Physics2D.OverlapCircleAll(pos, range, maskTarget);
		if(cols.Length > 0){
			Unit currentTarget = null;
			Unit targetTemp = null;
			
			float dist = Mathf.Infinity;
			Collider2D currentCollider = cols[0];
			foreach(Collider2D col in cols){
				float currentDist = Vector3.Distance(pos, col.transform.position);
				if(currentDist<dist){
					currentCollider = col;
					dist = currentDist;
				}
			}
			currentTarget = currentCollider.gameObject.GetComponent<Unit>();
			if(currentTarget!=null && currentTarget.HPAttribute.HP>0) target = currentTarget;
		}
	}

	IEnumerator ScanForTargetAllAround(){
		while (true) {
			Transform[] paths = SpawnManager.spawnManager.defaultPath.GetPath();
			//Vector3 alpha = (paths[wpCounter].position - paths[(wpCounter+1)%4].position)/11;

			Vector3 pos = thisPos;

			//thisObj.transform.position = pos;

			if(target == null){
				/*Collider2D[] cols = Physics2D.OverlapCircleAll(pos, range, maskTarget);
				if(cols.Length > 0){
					Unit currentTarget = null;
					Unit targetTemp = null;
					
					float dist = Mathf.Infinity;
					Collider2D currentCollider = cols[0];
					foreach(Collider2D col in cols){
						float currentDist = Vector3.Distance(pos, col.transform.position);
						if(currentDist<dist){
							currentCollider = col;
							dist = currentDist;
						}
					}
					currentTarget = currentCollider.gameObject.GetComponent<Unit>();
					if(currentTarget!=null && currentTarget.HPAttribute.HP>0) target = currentTarget;
				}*/
				ScanTarget(pos);
			}
			else{
				currentTargetDist=Vector3.Distance(thisPos, target.thisT.position);
				if(currentTargetDist>range || target.HPAttribute.HP<=0 || !target.thisObj.active){
					target=null;
				}
			}
			yield return null;
		}
	}
	
	IEnumerator TurretRoutine(){
		while (true) {
			if(target!=null){// && Vector3.Distance(thisPos, target.thisT.position)<range){

				/*Vector3 srcPos = thisT.position;

				tower.GetComponent<SpriteRenderer>().sprite = image[(target.wpCounter+3)%image.Length];
				GameObject obj = ObjectPoolManager.Spawn(shootObject, shootPoint.position, shootPoint.rotation);
				ShootObject shootObj = obj.GetComponent<ShootObject>();
				obj.SetActive(true);
				shootObj.Shoot(target, this, shootPoint);*/
				if (mState == State.Idle){
					mState = State.Attack;
					towerAnimator.enabled = true;
					tower.GetComponent<SpriteRenderer>().sprite = image[(target.wpCounter+3)%image.Length];
					towerAnimator.SetInteger("direction",(target.wpCounter+3)%4);

				}
				yield return new WaitForSeconds(Mathf.Max (0.05f, cooldown));
			}
			else{
				yield return null;
			}
		}
		//yield return new WaitForSeconds(1);
	}
	

	private void InitStat(){
		GameObject tempObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		tempObj.transform.localScale = new Vector3 (0.3f, 0.3f, 0.3f);
		tempObj.AddComponent<ShootObject>();
		tempObj.active=false;
		
		shootObject=tempObj;
		shootPoint = thisT;
	}

	IEnumerator HitTargetColor(UnitCreep tgt, Color color, float duration){
		Color temp = tgt.creep.GetComponent<SpriteRenderer> ().color; 
		tgt.creep.GetComponent<SpriteRenderer>().color = color;
		yield return new WaitForSeconds(duration);
		tgt.creep.GetComponent<SpriteRenderer>().color = tgt.getColor ();
		yield return null;
	}
	/*IEnumerator ApplySlow(UnitCreep tgt){
		float time = 0;
		Color color = new Color (111.0f / 255.0f, 177.0f / 255.0f, 255.0f / 255.0f, 1f);
		Color temp = tgt.creep.GetComponent<SpriteRenderer> ().color; 
		tgt.creep.GetComponent<SpriteRenderer>().color = color;
		tgt.isSlowed = true;
		tgt.currentMoveSpd = tgt.moveSpeed * (1 - slowPer); 
		GameObject a = ObjectPoolManager.Spawn(hitEffect, tgt.gameObject.transform.position + new Vector3(0, 0, -1), hitEffect.transform.rotation); 
		a.transform.parent = tgt.unitC.creep.transform;
		tgt.ApplyDamage(damage);
		yield return new WaitForSeconds(slowTime);
		tgt.isSlowed = false;
		tgt.currentMoveSpd = tgt.moveSpeed;
		tgt.creep.GetComponent<SpriteRenderer>().color = tgt.getColor();

		yield return null;
	
	}*/

	void ApplySlow(UnitCreep tgt){
		tgt.slowedTime = slowTime;
		tgt.slowPer = slowPer;
	}
	IEnumerator ApplyDot(UnitCreep tgt){
		float time = 0;
		//StartCoroutine(HitTargetColor(tgt, new Color(162.0f/255.0f, 190.0f/255.0f, 95.0f/255.0f, 1f), dotTotalTime));
		while(time < dotTotalTime){
			StartCoroutine(HitTargetColor(tgt, new Color(162.0f/255.0f, 190.0f/255.0f, 95.0f/255.0f, 1f), dotDuration));
			GameObject a = ObjectPoolManager.Spawn(hitEffect, tgt.gameObject.transform.position + new Vector3(0, 0, -1), hitEffect.transform.rotation); 
			a.transform.parent = tgt.creep.transform;
			tgt.ApplyDamage(damage);
			yield return new WaitForSeconds(dotDuration);
			time+=dotDuration;
		}
	}
	public void HitTarget(Vector3 pos, Unit tgt){
		if(tgt.gameObject!=null && tgt.gameObject.active){
			if (tgt.subClass == _UnitSubClass.Creep) {
				if(damage>0){

					if (isSlow){
						ApplySlow(tgt.unitC);
						GameObject a = ObjectPoolManager.Spawn(hitEffect, tgt.gameObject.transform.position + new Vector3(0, 0, -1), hitEffect.transform.rotation); 
						a.transform.parent = tgt.unitC.creep.transform;
						tgt.ApplyDamage(damage);
						AudioManager.PlayHitSound2();
					}
					else if (isDot){
						StartCoroutine(ApplyDot (tgt.unitC));
					}else{
						GameObject a = ObjectPoolManager.Spawn(hitEffect, tgt.gameObject.transform.position + new Vector3(0, 0, -1), hitEffect.transform.rotation); 
						a.transform.parent = tgt.unitC.creep.transform;
						StartCoroutine(HitTargetColor(tgt.unitC, new Color(1F, 125.0f/255.0F, 125.0f/255.0F, 1F), 0.3f));
						tgt.ApplyDamage(damage);
						AudioManager.PlayHitSound();
					}
				}
			}
		}
	}


	IEnumerator HitTargetColor(UnitCreep tgt){
		tgt.creep.GetComponent<SpriteRenderer>().color = new Color(1F, 125.0f/255.0F, 125.0f/255.0F, 1F);
		yield return new WaitForSeconds(0.2f);
		tgt.creep.GetComponent<SpriteRenderer>().color = Color.white;
		yield return null;
		
	}
	public void HitTarget(UnitCreep tgt, float damage){
		if(tgt.gameObject!=null && tgt.gameObject.active){
			if(damage>0){
				StartCoroutine(HitTargetColor(tgt.unitC));
				tgt.ApplyDamage(damage);
			}
		}
	}

	public void TurretAttack(){
		if (target==null) ScanTarget(thisPos);

		Unit tgt = target;
		if(tgt!=null && mState==State.Attack ){//&& Vector3.Distance(thisPos, target.thisT.position)<range ){
			if (warrior){
				HitTarget(Vector3.zero, tgt);
			}else{
				Vector3 srcPos = thisT.position;
				
				//tower.GetComponent<SpriteRenderer>().sprite = image[(target.wpCounter+3)%image.Length];
				
				GameObject obj;
				ShootObject shootObj;

				if (isSplash){
					int j;
					Collider2D[] cols = Physics2D.OverlapCircleAll(thisPos, splashRange, maskTarget);
					int l;
					if (cols.Length > splashCnt){
						l=splashCnt;
					}else{
						l=cols.Length;
					}

					for (j=0; j<cols.Length; j++){
						obj = ObjectPoolManager.Spawn(shootObject, shootPoint.position, shootPoint.rotation);
						shootObj = obj.GetComponent<ShootObject>();
						
						obj.SetActive(true);
						shootObj.Shoot(cols[j].gameObject.GetComponent<UnitCreep>(), this, shootPoint);
						
						//HitTarget(cols[j].gameObject.GetComponent<UnitCreep>(), splashRange);
					}
				}else{
					obj = ObjectPoolManager.Spawn(shootObject, shootPoint.position, shootPoint.rotation);
					shootObj = obj.GetComponent<ShootObject>();
					obj.SetActive(true);
					shootObj.Shoot(tgt, this, shootPoint);

				}
			}
		}
		mState = State.Idle;
		towerAnimator.enabled = false;


	}
}
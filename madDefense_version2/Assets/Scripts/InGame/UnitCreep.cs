﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class UnitCreep : Unit {

	public GameObject HpEffect;
	public GameObject Htemp;
	
	public delegate void LifeInHandler(int waveID);
	public static event LifeInHandler onLifeInE;
	
	public delegate void LifeDeHandler(int delta);
	public static event LifeDeHandler onLifeDeE;
	
	public GameObject creep;

	public bool isSemiBoss;
	public bool isBoss;
	public bool bossStart;

	private Animator creepAnimator;
	public Sprite[] image;
	
	public float moveSpeed=3;
	public int value;
	public String unitName;
	public int armor;
	public int magicResist;
	
	private int unitID;
	public float slowedTime;
	public float slowPer;

	public bool isFirst = true;
	public int weight = 1;

	[HideInInspector] public int waveID;
	public override void Awake () 
	{
		base.Awake ();
		SetSubClassInt(this);
		creepAnimator = creep.GetComponent<Animator> ();
	}
	public override void Start () {
		thisObj.layer=LayerManager.LayerCreep();
		base.Start();
		currentMoveSpd=moveSpeed;
		if (isBoss){
			creepAnimator.enabled = false;
			thisObj.GetComponent<CircleCollider2D>().enabled = false;
		}
		ObjectPoolManager.New(HpEffect, 1);
	}
	
	public void SetMoveSpeed(float moveSpd){
		moveSpeed=moveSpd;
	}
	
	public void Init(List<Vector3> waypoints, int uID, int wID){
		wpCounter = 0;
		this.UpdateZ ();
		base.Init ();
		unitID = uID;
		waveID = wID;
		wp=waypoints;
		currentMoveSpd=moveSpeed;
		
		if(onLifeDeE!=null) onLifeDeE(weight);
	}

	public void _bossStart(){
		if (isBoss) {
			bossStart = true;
			creepAnimator.enabled = true;
			thisObj.GetComponent<CircleCollider2D>().enabled = true;
		}
	}
	public override void Update () {
		base.Update();
		if (!isBoss) MoveWPMode();
		else{
			if (bossStart){
				MoveWPMode ();
			}
		}

		if (slowedTime > 0) {
			slowedTime -= Time.deltaTime;
			if (slowedTime < 0){
				creep.GetComponent<SpriteRenderer>().color = getColor();
			}else{
				Color color = new Color (111.0f / 255.0f, 177.0f / 255.0f, 255.0f / 255.0f, 1f);
				creep.GetComponent<SpriteRenderer>().color = color;
				currentMoveSpd = moveSpeed * (1 - slowPer); 
			}
		}else{
			currentMoveSpd = moveSpeed;
		}
	}

	void MoveWPMode(){
		//execute as long as there are unreached waypoint in the path
		if(wpCounter<wp.Count){
			//move to the next waypoint, if return true, then update to the next waypoint
			if(MoveToPoint(wp[wpCounter])){
				wpCounter+=1;
				creepAnimator.SetInteger("direction",(wpCounter+3)%4);
				//Debug.Log (wpCounter);
				//creep.GetComponent<SpriteRenderer>().sprite = image[wpCounter%4];
				// 0 : LF->RF, 1 : RF->RB, 2 : RB->LB, 3 : LB->L
				if ((wpCounter+3)%4==0){
					if (isFirst){
						isFirst = false;
					}else{
						GameObject a = ObjectPoolManager.Spawn(HpEffect, thisObj.transform.position + new Vector3(0, 0, -1), HpEffect.transform.rotation); 
						a.transform.parent = thisObj.transform;
						Htemp = a;
						this.GainHP(this.HPAttribute.fullHP*0.5f);
						AudioManager.PlayHeal();
					}
				}else if (wpCounter ==1){
					if (Htemp != null) ObjectPoolManager.Unspawn(Htemp);
				}
			}
		}
		else {
			wpCounter = 0;
		}
	}
	
	bool MoveToPoint(Vector3 point){
		float dist=Vector2.Distance(point, thisT.position);

		Vector3 temp1 = new Vector3 (point.x, point.y, 0);
		Vector3 temp2 = new Vector3 (thisT.position.x, thisT.position.y, 0);
		if(dist<0.15f) return true;
		Vector3 dir=(temp1-temp2).normalized;
		this.UpdateZ ();
		thisT.Translate(dir*Mathf.Min(dist, currentMoveSpd * Time.deltaTime), Space.World);

		return false;
	}
	
	public void Dead(){
		//Debug.Log ("dead");
		//GameControl.GainResource(value);
		slowedTime = 0;
		if (isSemiBoss) SpawnManager.spawnManager.semiBossDead = true;
		SpawnManager.spawnManager.totalUnitCount -= 1;
		GameControl.gameControl.GainItem(thisT, value);
		StartCoroutine(Unspawn(0.05f));
		if(onLifeInE!=null) onLifeInE(waveID);
	}
	IEnumerator Unspawn(float duration){
		currentMoveSpd = 0;
		while(creep.GetComponent<SpriteRenderer>().color.a > 0){
			slowedTime = 0;
			Color temp = creep.GetComponent<SpriteRenderer>().color;
			creep.GetComponent<SpriteRenderer>().color = new Color(temp.r, temp.g, temp.b, temp.a - 0.30f);
			yield return new WaitForSeconds(duration);
		}
		ObjectPoolManager.Unspawn(thisObj);
		yield return null;
	}

	public Color getColor(){
		if (slowedTime > 0) {
			return new Color (111.0f / 255.0f, 177.0f / 255.0f, 255.0f / 255.0f, 1f);
		}else{
			return Color.white;
		}
	}
}


﻿using UnityEngine;
using System.Collections;

public class SkillManager : MonoBehaviour {
	/* user skill */
	public GameObject thunderBolt;
	public static SkillManager skillManager;
	public float thunderBoltDamage;
	public float thunderBoltRange;
	public GameObject thunderBoltGauge;
	public float thunderBoltCoolTime;
	public bool thunderBoltAvailable;

	public PathTD path;

	public bool tornadoAvailable;
	public float tornadoCoolTime;
	public float tornadoNum;
	public float tornadoTime;
	public GameObject tornadoGauge;
	public GameObject tornado;

	void InitSkill(){
		skillManager = this;
		ObjectPoolManager.New (thunderBolt, 20);
		ObjectPoolManager.New (tornado, 12);
		thunderBoltAvailable = true;
		tornadoAvailable = true;
	}
	Vector3 GetPos(Vector3 point1, Vector3 point2){
		
		Vector3 temp1 = new Vector3 (point1.x, point1.y, 0);
		Vector3 temp2 = new Vector3 (point2.x, point2.y, 0);

		Vector3 dir=(temp1-temp2);

		float a = Random.Range (0.0f, 1.0f);
		//Vector3 result = point1 + a * (dir);
		//Debug.Log (a);
		Vector3 v = point1 - a * (dir);
		return new Vector3 (v.x, v.y, v.y);
	}
	IEnumerator HitTargetColor(UnitCreep tgt){
		tgt.creep.GetComponent<SpriteRenderer>().color = new Color(1F, 125.0f/255.0F, 125.0f/255.0F, 1F);
		yield return new WaitForSeconds(0.2f);
		tgt.creep.GetComponent<SpriteRenderer>().color = Color.white;
		yield return null;
		
	}
	public void HitTarget(UnitCreep tgt, float damage){
		if(tgt.gameObject!=null && tgt.gameObject.active){
			if(damage>0){
				StartCoroutine(HitTargetColor(tgt.unitC));
				tgt.ApplyDamage(damage);
				AudioManager.PlayHitSound();
			}
		}
	}

	IEnumerator ThunderBoltShot(){
		Transform[] waypoints = path.GetPath ();
		int i, j;
		for (i=0; i<15; i++) {
			Vector3 pos = GetPos (waypoints[i%4].position, waypoints[(i+1)%4].position);
			pos += new Vector3(0, 0, pos.y+100f);
			GameObject temp = ObjectPoolManager.Spawn(thunderBolt, pos, thunderBolt.transform.rotation);
			temp.GetComponent<ParticleSystem>().startSize *= Random.Range (1.0f, 2.0f);
			temp.GetComponent<ParticleSystem>().Play();
			LayerMask maskTarget = 1 << LayerManager.LayerCreep(); 
			Collider2D[] cols = Physics2D.OverlapCircleAll(temp.transform.position, thunderBoltRange, maskTarget);
			for (j=0; j<cols.Length; j++){
				HitTarget(cols[j].gameObject.GetComponent<UnitCreep>(), thunderBoltDamage);
			}
			yield return new WaitForSeconds(0.1f);
		}



		yield return null;
	}
	IEnumerator ThunderBoltSkillCoolTime(GameObject Gauge, float cooltime){
		float time = 0;
		Gauge.GetComponent<UI2DSprite> ().fillAmount = 1.0f;
		while (time < cooltime){
			time += Time.deltaTime;
			Gauge.GetComponent<UI2DSprite> ().fillAmount = 1.0f - time/thunderBoltCoolTime;
			yield return null;
		}
		thunderBoltAvailable = true;
		yield return null;
	}
	public void ThunderBoltClick(){
		if (thunderBoltAvailable){
			thunderBoltAvailable = false;
			StartCoroutine (ThunderBoltShot ());
			StartCoroutine (ThunderBoltSkillCoolTime(thunderBoltGauge, thunderBoltCoolTime));
		}
	}

	IEnumerator tornadoRun(){
		int l = BuildManager.buildManager.platforms.Length;
		int i;
		GameObject[] a = new GameObject[12];
		for (i=0; i<l; i++){
			UnitTower t = BuildManager.buildManager.platforms [i].GetComponent<Platform> ().tower;
			if (t!=null){
				a[i] = ObjectPoolManager.Spawn(tornado, t.HPAttribute.overlayHP.position, tornado.transform.rotation);
				a[i].GetComponent<ParticleSystem>().Play();
				t.towerAnimator.speed = 3f;
				t.upAttackSpeed (tornadoNum);
				t.tornadoEffect = a[i];
			}else{
				a[i] = null;
			}
		}
		yield return new WaitForSeconds(tornadoTime);

		for (i=0; i<l; i++){
			UnitTower t = BuildManager.buildManager.platforms [i].GetComponent<Platform> ().tower;
			if (t!=null){
				if (t.towerAnimator.speed == 3f){
					t.tornadoEffect = null;
					ObjectPoolManager.Unspawn(a[i]);
					t.towerAnimator.speed = 1f;
					t.upAttackSpeed (0f);
					a[i]=null;
				}
			}
		}

		yield return null;
	}

	IEnumerator tornadoSkillCoolTime(GameObject Gauge, float cooltime){
		float time = 0;
		Gauge.GetComponent<UI2DSprite> ().fillAmount = 1.0f;
		while (time < cooltime){
			time += Time.deltaTime;
			Gauge.GetComponent<UI2DSprite> ().fillAmount = 1.0f - time/tornadoCoolTime;
			yield return null;
		}
		tornadoAvailable = true;
		yield return null;
	}

	public void TornadoClick(){
		if (tornadoAvailable) {

			tornadoAvailable = false;
			StartCoroutine (tornadoRun());
			StartCoroutine (tornadoSkillCoolTime(tornadoGauge, tornadoCoolTime));
		}

	}
	// Use this for initialization
	void Start () {
		InitSkill ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;


//using System.Collections.Generic;

public class UI : MonoBehaviour {
	public Camera MainCamera;
	public Camera NGUICamera;
	public Camera ViewCamera;
	
	private bool towerInfoShow = false;
	public GameObject curInfo;
	private GameObject curTower;
	public bool clickEnable;
	
	public static UI ui;
	private bool enableSpawnButton=true;
	private int w;
	private int h;
	public static bool winLostFlag = false;
	
	
	public UILabel MoneyCount;
	public UILabel WaveCount;
	public UISlider LifeCount;
	public GameObject LifeCount2;
	
	public bool paused;
	public bool bossClick;
	public GameObject GameEnd;
	public GameObject bagButton;
	public GameObject railtop;
	public GameObject railbottom;
	public GameObject circleGauge;
	public CameraShake cShake;

	public Sprite[] buttonImg; // 0 : PlayButton 1: PlayClick 2: PauseButton 3: PauseClick
	public SpriteRenderer playButton;
	public bool isSpawnClick = false;

	public bool lifeLabel;
	public GameObject PauseInfo;
	public GameObject upgrade;

	void Awake(){
		ui=this;
		Init ();
	}
	void OnEnable(){
	}
	void OnDisable(){
	}
	
	
	
	void Init(){
		winLostFlag = false;
		towerInfoShow = false;
		clickEnable = true;
		paused = false;
		bossClick = false;

		//setCircleGauge ();
	}

	void setCircleGauge(){
		GameObject o = circleGauge;
		//curTower.transform.Find("info").transform.localScale = Vector3.one;
		//Vector3 temp = curTower.transform.TransformPoint(curTower.GetComponent<UnitTower>().tower.transform.localPosition);
		Vector3 temp = new Vector3 (-5.56f, -0.07f, 0.0f);
		temp = Camera.main.WorldToScreenPoint(temp) - new Vector3(Screen.width/2, Screen.height/2, 0);
		Debug.Log (Screen.height);
		temp = new Vector3(temp.x*720.0f/Screen.height, temp.y*720.0f/Screen.height, 0);
		temp.z = 10;
		o.transform.localPosition = temp;
	}
	void Start () 
	{
		w = Screen.width;
		h = Screen.height;
	}
	//called when SpawnManager clearFor Spawing event is detected, enable spawnButton
	void OnClearForSpawning(bool flag){
		enableSpawnButton=flag;
	}

	public void pauseClick(){
		if (bossClick){
			if (Time.timeScale == 0) Time.timeScale = 1;
			else Time.timeScale = 0;
			return;
		}
		paused = !paused;
		if (paused){
			PauseInfo.SetActive (true);
			ViewCamera.gameObject.SetActive(false);
			Time.timeScale = 0;
		}else{
			ViewCamera.gameObject.SetActive(true);
			PauseInfo.SetActive (false);
			Time.timeScale = 1;
		}
	}
	// bagClick -> goto bag
	public void bagClick(){
		if (paused) return;
		bagButton.SetActive(false);
		towerInfoShow = false;
		curInfo.transform.localScale = Vector3.zero;
		towerInfoShow = false;

		int i;
		GameControl.gameControl.AddTower (curTower.GetComponent<UnitTower> ().GetTowerID ());
		curTower.GetComponent<UnitTower>().Destroy();
		GameControl.gameControl.refreshTip ();
	}
	void backClick(){
		if (paused) return;

		GameControl.gameControl.backTip ();
	}
	void SpawnClick(){
		if (paused) return;
		AudioManager.PlayPlayClickSound ();
		if(enableSpawnButton){
			if(GameControl.gameState!=_GameState.Ended){
				//if spawn is successful, disable the spawnButton
				if(SpawnManager.Spawn()){
					enableSpawnButton=false;
					railtop.GetComponent<Animator>().SetBool("spawn", true);
					railbottom.GetComponent<Animator>().SetBool("spawn", true);
				}
			}
		}

		if (bossClick){
			bossClick = false;
			ViewCamera.gameObject.SetActive(false);
			paused = true;
		}
	}
	void ThunderBoltClick(){
		if (paused) return;
		SkillManager.skillManager.ThunderBoltClick ();
	}

	public void TornadoClick(){
		if (paused) return;
		SkillManager.skillManager.TornadoClick ();
	}
	IEnumerator railDrop(){
		Debug.Log ("aaaa");
		Vector3 rt = railtop.transform.localPosition;
		Vector3 rb = railbottom.transform.localPosition;
		while (rt.y <= 0f && rb.y <= 0f ){
			Debug.Log (rt);
			Debug.Log (rb);
			railtop.transform.localPosition = new Vector3(rt.x, rt.y+0.1f, rt.z);
			railbottom.transform.localPosition = new Vector3(rb.x, rb.y+0.1f, rb.z);
			
			rt = railtop.transform.localPosition;
			rb = railbottom.transform.localPosition;
			yield return new WaitForSeconds(0.05f);
		}
		yield return new WaitForSeconds (0.5f);
		while (rt.y > -20f && rb.y > -20f ){
			Debug.Log (rt);
			Debug.Log (rb);
			railtop.transform.localPosition = new Vector3(rt.x, rt.y-0.5f, rt.z);
			railbottom.transform.localPosition = new Vector3(rb.x, rb.y-0.5f, rb.z);
			
			rt = railtop.transform.localPosition;
			rb = railbottom.transform.localPosition;
			yield return new WaitForSeconds(0.01f);
		}
		yield return null;
	}
	
	IEnumerator gameOverAnimation(){
		float shake = cShake.shakeAmount;
		GameObject result = null;
		upgrade.SetActive (false);
		GameEnd.transform.Find ("GameOverInfo").gameObject.SetActive (true);
		AudioManager.MusicStop ();
		cShake.stop ();
		if (winLostFlag)
			result = GameEnd.transform.Find ("PassObject").gameObject;
		else
			result = GameEnd.transform.Find ("FailObject").gameObject;
		result.SetActive (true);
		Transform tf = result.transform;
		UI2DSprite sp = result.GetComponent<UI2DSprite>();
		sp.color = new Color (sp.color.r, sp.color.g, sp.color.b, 0);
		sp.SetRect (-190f, 0f, 2800f, 2400f);
		float timezero = 0.0f;
		while(timezero < 2.0f){
			timezero += Time.deltaTime;
			sp.color = new Color (255, 255, 255, timezero/2);
			sp.SetRect(-190f, 0f, 2800 - timezero*1225, 2400 - timezero*1050);
			yield return null;
		}
		Time.timeScale = 0;
		paused = true;
		ViewCamera.gameObject.SetActive(false);
		yield return new WaitForSeconds(10.0f);
		Application.LoadLevel ("StageScene2");
	}
	
	public void GameOver(){
		lifeLabel = false;
		AudioManager.warningSoundStop();
		StartCoroutine (gameOverAnimation ());
	}
	
	IEnumerator showInfo(Transform info){
		clickEnable = false;
		while (info.transform.localScale.x <30) {
			info.transform.localScale += new Vector3(0.2f*30, 0.2f*30, 1f);
			yield return new WaitForSeconds(0.05f);
		}
		//yield return new WaitForSeconds(0.5f);
		towerInfoShow = true;
		clickEnable = true;
		yield return null;
	}
	
	
	
	
	void OnGUI(){
		if (paused) return;
		int buttonX=8;
		
		/*
		if(enableSpawnButton){
			if(GUI.Button(new Rect(8, 5, w/10, h/10 - 8), "Spawn")){
				//if the game is not ended
				if(GameControl.gameState!=_GameState.Ended){
					//if spawn is successful, disable the spawnButton
					if(SpawnManager.Spawn())
						enableSpawnButton=false;
				}
			}
			buttonX+=w/10 + 5;
		}
		*/
		int resource=GameControl.GetResourceVal();
		MoneyCount.text = resource.ToString();
		WaveCount.text = SpawnManager.GetCurrentWave () + " / " + SpawnManager.GetTotalWave ();
		GameControl g = GameControl.gameControl;
		
		if (lifeLabel){
			LifeCount.value = (g.maxPlayerLife- GameControl.GetPlayerLife ()) * 1.0f / g.maxPlayerLife;
			float x = -225f+((g.maxPlayerLife- GameControl.GetPlayerLife ()) * 1.0f / g.maxPlayerLife)*450f;
			LifeCount2.transform.localPosition = new Vector3(x, 0, 0);
		}
	
		if (lifeLabel && LifeCount.value > 0.7f) {
			float shakeAmount = LifeCount.value - 0.7f;
			//Debug.Log(LifeCount.value - 0.6f);
			AudioManager.PlayWarning();
			cShake.run (0.06f + shakeAmount/2.5f);
		}else{
			if (lifeLabel){
				AudioManager.warningSoundStop();
				cShake.stop ();
			}
		}
		LayerMask maskTower = 1 << LayerManager.LayerTower ();
		
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity, maskTower);
		
		LayerMask maskNGUI = 1 << LayerManager.LayerNGUI ();
		
		Ray ray2 = NGUICamera.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit2;

		LayerMask maskSpawnButton = 1 << LayerManager.LayerSpawnButton ();
		
		Ray ray3 = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit3 = Physics2D.GetRayIntersection(ray3, Mathf.Infinity, maskSpawnButton);

		
		if (curTower==null){
			bagButton.SetActive(false);
		}
		if (Input.GetMouseButtonDown (0)) {
			if (hit3.collider != null){
				if (enableSpawnButton){
					isSpawnClick = true;
					SpawnClick();
					enableSpawnButton = false;
					playButton.sprite = buttonImg[1];
				}else{
					isSpawnClick = true;
					playButton.sprite = buttonImg[3];
					//pauseClick();
				}
			}
			
			
			if (Physics.Raycast(ray2, out hit2, Mathf.Infinity, maskNGUI)) {
				//Debug.Log ("aa");
			}else{
				if (towerInfoShow){
					towerInfoShow = false;
					curTower.GetComponent<UnitTower>().tower.GetComponent<SpriteRenderer>().color = Color.white;
					bagButton.SetActive(false);
				}
				if (hit.collider != null) {
					curTower=hit.collider.transform.gameObject;
					if (curTower != null && curTower.GetComponent<UnitTower>().isBuilt){
						bagButton.SetActive(true);
						GameControl.gameControl.showTip(curTower.GetComponent<UnitTower>().GetTowerID (), 1);

						AudioManager.PlayTowerClick ();

						curTower.GetComponent<UnitTower>().tower.GetComponent<SpriteRenderer>().color = Color.yellow;
						towerInfoShow = true;
					}
				}
			}
		}
		if (isSpawnClick && Input.GetMouseButtonUp (0)) {
			if (hit3.collider != null){
				isSpawnClick = false;
				playButton.sprite = buttonImg[2];
			}
		}

		/*
		if (Input.GetMouseButtonDown(0) && clickEnable){

			if (towerInfoShow){


				//Debug.Log ("aa");
				if (UICamera.hoveredObject ==null){
					//Debug.Log ("bb");
					curInfo.transform.localScale = Vector3.zero;
					towerInfoShow = false;
				}else{
					//Debug.Log (UICamera.hoveredObject.transform.name);
					if (UICamera.hoveredObject.transform.name.Equals("View UI")){
						curInfo.transform.localScale = Vector3.zero;
						towerInfoShow = false;
					}
					else if (UICamera.hoveredObject.transform.parent != null){

						if (!UICamera.hoveredObject.transform.parent.name.Equals("info")){
							curInfo.transform.localScale = Vector3.zero;
							towerInfoShow = false;
						}
					}
				}
			}else{
				if (hit.collider != null) {
					curInfo.transform.localScale = Vector3.zero;
					curTower=hit.collider.transform.gameObject;
					if (curTower != null && curTower.GetComponent<UnitTower>().isBuilt){
						curTower.transform.Find("info").transform.localScale = Vector3.one;
						Vector3 temp = curTower.transform.TransformPoint(curTower.GetComponent<UnitTower>().tower.transform.localPosition);
						temp = Camera.main.WorldToScreenPoint(temp) - new Vector3(Screen.width/2, Screen.height/2, 0);
						temp = new Vector3(temp.x*720.0f/Screen.height, temp.y*720.0f/Screen.height, 0);
						temp.z = 10;
						curInfo.transform.localPosition = temp;
						GameControl.gameControl.showTip(curTower.GetComponent<UnitTower>().GetTowerID (), 1);
						StartCoroutine (showInfo(curInfo.transform));
					}
				}
			}

		}*/
		//BuildMenuAllTowersFix ();
		
		
		
		if (GameControl.gameState != _GameState.Ended) {
		} else {
		}
	}
}
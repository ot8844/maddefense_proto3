﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {
	public int towerID;
	public UILabel cnt;

	public void ButtonDestroy(){
		UITable a = this.transform.parent.GetComponent<UITable> ();
		Destroy (this.gameObject);
		a.Reposition ();
	}

	public void LabelCount(int count){
		cnt.text = "x" + count.ToString ();
	}
	void ItemClick(){
		UnitTower[] towerList = BuildManager.GetTowerList ();
		UnitTower tower = towerList[towerID];
		UI.ui.clickEnable = false;
		for (int i =0; i< BuildManager.buildManager.platforms.Length; i++){
			UnitTower towert = BuildManager.buildManager.platforms[i].gameObject.GetComponent<Platform>().tower;
			if (towert!=null){
				Color c = towert.tower.GetComponent<SpriteRenderer>().color;
				towert.tower.GetComponent<SpriteRenderer>().color = new Color(c.r, c.g, c.b, 0.4f);

			}
			
		}

		BuildManager.BuildTowerDragNDrop (tower, this);
		GameControl.gameControl.showTip (towerID, 2);
	}
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

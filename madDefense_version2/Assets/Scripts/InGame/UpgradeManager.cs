﻿using UnityEngine;
using System.Collections;

public class UpgradeManager : MonoBehaviour {
	public GameObject upList1;
	public GameObject upList2;
	public UILabel upButton;
	public int MaxLevel = 5;
	public static UpgradeManager upgradeManager;

	int[,] UpgradeLevel = new int[3,3];

	// Use this for initialization
	void Start () {
		Init ();
		upgradeManager = this;
	}
	void Init(){
		for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
				UpgradeLevel[i, j] = 0;
			}
		}
	}
	public void updateUpgrade(UnitTower tower){
		tower.upDamage (UpgradeLevel [tower.getClassNumber (), 0] * 0.2f);
		tower.upAPDamage (UpgradeLevel [tower.getClassNumber (), 1] * 0.2f);
		tower.upAttackSpeed (UpgradeLevel [tower.getClassNumber (), 2] * 0.1f);
	}

	public void upgradeShow(UnitTower tower){
		GameObject upgrade = tower.transform.Find ("upgrade").gameObject;
		GameObject ad = upgrade.transform.Find ("AD").gameObject;
		GameObject ap = upgrade.transform.Find ("AP").gameObject;
		GameObject sp = upgrade.transform.Find ("AS").gameObject;

		upgrade.SetActive (true);
		if (UpgradeLevel[tower.getClassNumber(), 0] == 0){
			ad.SetActive(false);
		}else{
			ad.SetActive(true);
			float g = 1.0f - (UpgradeLevel[tower.getClassNumber(), 0]-1)*1.0f / (MaxLevel-1);
			ad.GetComponent<SpriteRenderer>().color = new Color(1.0f, g, 0.0f, 0.7f);
		}

		if (UpgradeLevel[tower.getClassNumber(), 1] == 0){
			ap.SetActive(false);
		}else{
			ap.SetActive(true);
			float g = 1.0f - (UpgradeLevel[tower.getClassNumber(), 1]-1)*1.0f / (MaxLevel-1);
			ap.GetComponent<SpriteRenderer>().color = new Color(1.0f, g, 0.0f, 0.7f);;
		}

		if (UpgradeLevel[tower.getClassNumber(), 2] == 0){
			sp.SetActive(false);
		}else{
			sp.SetActive(true);
			float g = 1.0f - (UpgradeLevel[tower.getClassNumber(), 2]-1)*1.0f / (MaxLevel-1);
			sp.GetComponent<SpriteRenderer>().color = new Color(1.0f, g, 0.0f, 0.7f);
		}

		updateUpgrade (tower);
	}

	public void upgradeClick(){
		GameObject a = upList1.GetComponent<UICenterOnChild> ().centeredObject;
		GameObject b = upList2.GetComponent<UICenterOnChild> ().centeredObject;
		int j = indexReturn (a.name);
		int i = indexReturn (b.name);
		if (UpgradeLevel [i, j] == MaxLevel)
			return;

		int resource = GameControl.GetResourceVal ();
		if (resource >= UpgradeLevel[i,j]+1){
			GameControl.SpendResource(UpgradeLevel[i,j]+1);
			UpgradeLevel[i,j]++;
		}
		for (int k=0; k<BuildManager.buildManager.platforms.Length; k++){
			UnitTower t = BuildManager.buildManager.platforms [k].GetComponent<Platform> ().tower;
			if (t!= null) upgradeShow(t);
		}

	}
	int indexReturn(string s){
		if (s.Equals("AD") || s.Equals("HUMAN")){
			return 0;
		}else if(s.Equals("AP") || s.Equals("ANIMAL")){
			return 1;
		}else{
			return 2;
		}
	}

	void changeUI(){
		GameObject a = upList1.GetComponent<UICenterOnChild> ().centeredObject;
		GameObject b = upList2.GetComponent<UICenterOnChild> ().centeredObject;
		
		upButton.text = UpgradeLevel [indexReturn (b.name), indexReturn (a.name)].ToString ();
	}
	// Update is called once per frame
	void Update () {
		changeUI ();
	}
}

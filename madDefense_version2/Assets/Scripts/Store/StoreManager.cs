﻿using UnityEngine;
using System.Collections;

public class StoreManager : MonoBehaviour {
	public TowerItem[] item ;
	int currentPage;
	public GameObject[] itemBox; 
	public GameObject pageNum;
	int currentLevel;
	int currentTower;

	public int[] currentID = new int[10];

	public static StoreManager storeManager;

	void Init(){
		for (int i=0; i<20; i++){
			if (ServerManager.unit[i] == '1'){
				item[i].selled = true;
			}
		}
	}
	// Use this for initialization
	void Start () {
		Init ();
		currentPage = 0;
		currentLevel = 0;
		currentTower = -1;
		pageUpdate (0);
		storeManager = this;
	}
	void numberUpdate(){
		pageNum.GetComponent<UILabel> ().text = (currentPage+1).ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		numberUpdate ();
	}
	void pageUpdate(int pageNumber){
		int i;
		currentPage = pageNumber;

		if (currentLevel == 0){
			for (i=0; i<10; i++){
				if (currentPage*10+i >= item.Length){
					itemBox[i].SetActive(false);
				}else{
					itemBox[i].SetActive (true);
					if (item[currentPage*10+i].image != null){
						itemBox[i].transform.Find("Tower").GetComponent<UI2DSprite>().sprite2D = item[currentPage*10+i].image;
						if (!item[currentPage*10+i].selled){
							itemBox[i].transform.Find("Tower").GetComponent<UI2DSprite>().color = Color.black;
						}else{
							itemBox[i].transform.Find("Tower").GetComponent<UI2DSprite>().color = Color.white;
						}
					}
					if (item[currentPage*10+i].towerMoney != 0){
						itemBox[i].transform.Find("Price").GetComponent<UILabel>().text = item[currentPage*10+i].towerMoney.ToString() + "$";
					}
					if (item[currentPage*10+i].level != 0){
						itemBox[i].transform.Find("level").GetComponent<UILabel>().text = item[currentPage*10+i].level.ToString();
				
					}

					currentID[i] = currentPage*10+i;

				}
			}
		}else{

			int cntLevel = currentPage*10;
			int cnt = 0;
			int cnt2 = 0;
			int j=0;
			while (true){
				if (j >= item.Length) break;
				if (item[j].level == currentLevel){
					//Debug.Log (cnt);
					if (cntLevel <= cnt && cntLevel+10 > cnt){
						cnt2++;
						i = cnt-cntLevel;
						itemBox[i].SetActive (true);
						if (item[j].image != null){
							itemBox[i].transform.Find("Tower").GetComponent<UI2DSprite>().sprite2D = item[j].image;
							if (!item[j].selled){
								itemBox[i].transform.Find("Tower").GetComponent<UI2DSprite>().color = Color.black;
							}else{
								itemBox[i].transform.Find("Tower").GetComponent<UI2DSprite>().color = Color.white;
							}
						}
						if (item[j].towerMoney != 0){
							itemBox[i].transform.Find("Price").GetComponent<UILabel>().text = item[j].towerMoney.ToString() + "$";
						}
						if (item[j].level != 0){
							itemBox[i].transform.Find("level").GetComponent<UILabel>().text = item[j].level.ToString();
							
						}

						currentID[i] = j;
					}
					cnt++;
				}
				j++;
			}


			for (i=cnt2; i<10; i++){
				itemBox[i].SetActive(false);
			}
		}


	}

	public void currentSelect(int a){
		currentTower = currentID [a];
	}

	public void showOnlyLevel(int level){
		currentLevel = level;
		currentPage = 0;
		pageUpdate (0);
	}

	public void leftUpdate(){
		if (currentPage !=0){
			currentPage--;
			pageUpdate (currentPage);
		}
	}

	public void rightUpdate(){
		currentPage++;
		pageUpdate (currentPage);
	}


}

[System.Serializable]
public class TowerItem{
	public int towerMoney = 0;
	public int level = 0;
	public Sprite image = null;
	public bool selled = false;
	public string name = null;
}

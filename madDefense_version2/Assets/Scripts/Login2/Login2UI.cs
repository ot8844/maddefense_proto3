﻿using UnityEngine;
using System.Collections;

public class Login2UI : MonoBehaviour {

	public static Login2UI login2UI;
	public UILabel Name;
	public UILabel Money;
	public UILabel Stage;
	int currentStage = 1;

	void Init(){
		ServerManager.serverManager.unitInfo (ServerManager.cid);
		ServerManager.serverManager.moneyInfo (ServerManager.cid);
		Stage.text = currentStage.ToString ();
		login2UI = this;
	}
	// Use this for initialization
	void Start () {
		Init ();
	}
	
	// Update is called once per frame
	void Update () {
		Name.text = ServerManager.cid;
		Money.text = ServerManager.money;
	}

	public void storeClick(){
		Application.LoadLevel ("store");
	}

	public void prevClick(){
		if (currentStage > 1) 
			currentStage--;
		Stage.text = currentStage.ToString ();
	}
	public void nextClick(){
		currentStage++;
		Stage.text = currentStage.ToString ();
	}

	public void startClick(){
		Application.LoadLevel ("InGameSceneFour");
	}
}

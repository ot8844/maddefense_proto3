﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour {
	public UISlider loadingBar;

	// Use this for initialization
	void Start () {

	}
	IEnumerator loadScene(){
		AsyncOperation async = Application.LoadLevelAsync ("ProtoSceneTwo");
		loadingBar.transform.gameObject.SetActive (true);
		loadingBar.value = 0.0f;
		while(!async.isDone) 
		{
			loadingBar.value = async.progress;
			yield return null;
		}
		loadingBar.value = 1.0f;
		yield return null;
	}

	public void easy(){
		SpawnManager.stagelevel = 1.0f;
		Application.LoadLevel ("ProtoSceneTwo");
	}

	public void normal(){
		SpawnManager.stagelevel = 1.2f;
		Application.LoadLevel ("ProtoSceneTwo");
	}

	public void hard(){
		SpawnManager.stagelevel = 1.5f;
		Application.LoadLevel ("ProtoSceneTwo");
	}
	// Update is called once per frame
	void Update () {
	
	}
}
